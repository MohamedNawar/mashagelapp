//
//  CountryMain.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//


import Foundation
import Alamofire
import PKHUD

class CityMain {
    static let Instance = CityMain()
    var cityData = Cities()
    private init() {}
    
    public func getCitiesNameArr() -> [String]{
        var temp = [String]()
        for item in (CityMain.Instance.cityData.data ?? [CitiesData]() ){
            temp.append(item.name ?? "")
        }
        return temp
        
    }
    public func getCitiesServer(enterDoStuff: @escaping () -> Void)  {
        
        let header = APIs.Instance.getHeader()
        print(header)
        print(APIs.Instance.showAreas())
        Alamofire.request(APIs.Instance.showAreas(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            print(response.result.value)
            switch(response.result) {
            case .success(let value):
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print(temp)
                }else{
                    
                    do {
                        
                        self.cityData = try JSONDecoder().decode(Cities.self, from: response.data!)
                         print(self.cityData)
                    }catch{
                        HUD.flash(.labeledError(title: "", subtitle: "حدث خطأ برجاء اعادة المحاولة"), delay: 1.0)
                        print("cannot parse")
                    }
                }
                enterDoStuff()
            case .failure( _):
                enterDoStuff()
            }
        }
        
        
    }
    
    
}

class Cities : Decodable {
    var data : [CitiesData]?
}
class CitiesData : Decodable {
    func getAreasName() -> [String] {
        var temp = [String]()
        for item in self.areas!{
            temp.append(item.name ?? "")
        }
        return temp
    }
    var id : Int?
    var name : String?
    var areas : [Areas]?
}
class Areas : Decodable {
    var id : Int?
    var name : String?
    var city_id : Int?
}
