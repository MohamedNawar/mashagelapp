//
//  userData.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation
struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    var data : UserDetails?
    var token : String?
    var identifierInside : Int?
    var identifierOutside : Int?
    var userSelectedShopId : Int?
    var currentEmployeeId :Int?
    var selectedDate :String?
    var serviceType : String?
    var currentReserveShopId : Int? = -1
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "identifierInside")
        UserDefaults.standard.removeObject(forKey: "identifierOutside")
        UserDefaults.standard.removeObject(forKey: "userSelectedShopId")
        UserDefaults.standard.removeObject(forKey: "currentEmployeeId")
        UserDefaults.standard.removeObject(forKey: "selectedDate")
        UserDefaults.standard.removeObject(forKey: "serviceType")
        UserDefaults.standard.removeObject(forKey: "currentReserveShopId")

        token = ""
    }
    mutating func removecurrentReserveShopId() {
        UserDefaults.standard.removeObject(forKey: "currentReserveShopId")
    }
    mutating func removeidentifierOutside() {
        UserDefaults.standard.removeObject(forKey: "identifierOutside")
    }
    mutating func removeIdentifierInside() {
        UserDefaults.standard.removeObject(forKey: "identifierInside")
    }
    mutating func removeCurrentEmployeeId() {
        UserDefaults.standard.removeObject(forKey: "currentEmployeeId")
    }
    mutating func removeShopID() {
        UserDefaults.standard.removeObject(forKey: "userSelectedShopId")
    }
    mutating func removeselectedDate() {
        UserDefaults.standard.removeObject(forKey: "selectedDate")
    }
    mutating func removeserviceType() {
        UserDefaults.standard.removeObject(forKey: "serviceType")
    }
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
                print("gg")
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "identifierInside") {
            self.identifierInside = Int(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "identifierOutside") {
            self.identifierOutside = Int(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "userSelectedShopId") {
            self.userSelectedShopId = Int(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "currentEmployeeId") {
            self.currentEmployeeId = Int(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "selectedDate") {
            self.selectedDate = String(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "serviceType") {
            self.serviceType = String(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "currentReserveShopId") {
            self.currentReserveShopId = Int(data)
        }
    }
}

struct employeeData : Decodable  {
    var data : UserDetails?
}
struct employeesData : Decodable  {
    var data : [UserDetails]?
}
struct UserDetails : Decodable {
    var id:Int?
    var name : String?
    var email:String?
    var mobile:String?
    var type:Int?
    var personal_id: String?
    var is_superadmin: Bool?
    var addresses : [Addresses]?
    var links: UserDetailslinks?
    var city : CiyDataInformation?
    var latitude : String?
    var longitude : String?
    var certifications : [String]?
    var issues : [String]?
    
    var address:String?
    
}
class Addresses : Decodable {
    var id : Int?
    var fullname : String?
    var city : CiyDataInformation?
    var name : String?
    var phone : String?
    var address : String?
    var is_default : Bool?
    var shipping_price : Shipping_Price?
    var total_with_shipping_price : Shipping_Price?
    var links:Links?
    
}
class Shipping_Price : Decodable {
    var amount : Int?
    var currency : String?
    var amount_formated : String?
}
class Links : Decodable {
    var update_address = linksContent()
}
class linksContent : Decodable {
    var href : String?
    var method : String?
}
class UserDetailslinks : Decodable {
    var update_profile : linksContent?
    var add_address : linksContent?
}
class CityData : Decodable {
    var data = CiyDataInformation()
}
class CiyDataInformation : Decodable {
    var id : Int?
    var name : String?
    var country : CountriesDataInformation?
}
class CountriesDataInformation : Decodable {
    var id : Int?
    var `default` : Int?
    var name : String?
    
}


