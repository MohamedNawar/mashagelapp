//
//  s.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation

class ErrorHandler : Decodable{
    var message : String?
    var errors : ErrorTypes?
    func parseError() -> String {
        var str = ""
        if let temp = self.errors?.email{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.name{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.password{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.avatar{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.address{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.city_id{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.mobile{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.qty{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.date{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        if let temp = self.errors?.employee_id{
            if temp.count != 0{
                str.append(temp[0])
                str.append("\n")
                
            }
        }
        return str
    }
    
    
}
struct ErrorImd : Decodable {
    var errors : ErrorTypes?
}
struct ErrorTypes : Decodable {
    var email : [String]?
    var password : [String]?
    var name : [String]?
    var avatar : [String]?
    var city_id : [String]?
    var address : [String]?
    var mobile:[String]?
    var qty:[String]?
    var date:[String]?
    var employee_id : [String]?
    
    
}

