//
//  Component.swift
//  mashagel
//
//  Created by MACBOOK on 10/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation


class Shops : Decodable {
    var data : [ShopData]?
}
class Shop: Decodable {
    var data : ShopData?
}


class Employee : Decodable {
    var data : EmployeeData?
}
class CartContent : Decodable {
    var items : [ServicesData]?
    var package_items : [Package_item]?
}
class Package_item : Decodable {
    var id : Int?
    var name : String?
    var image : [Images]?
    var price : String?
    var type : QuantumValue?
    var type_label : String?
    var description : String?
    var short_description : String?
    var employee_name: String?
    var employee_id : Int?
    var due_time:String?
    var assigned_employee_name:String?
    var assigned_employee_id: Int?
    var employees:[EmployeeData]?
    var services:[ServiceData]?
    var items : [ServicesData]?
}
class EmployeeData : Decodable {
    var id : Int?
    var name : String?
    var mobile : String?
    var email : String?
    var personal_id : String?
    var type : QuantumValue?
    var services : [ServicesData]?
    var image :Images?
    var employee_id : Int?
    var employee_name : String?
    var times : QuantumValue?
}

class ShopData : Decodable {
    var id : Int?
    var name : String?
    var mobile : String?
    var email : String?
    var latitude : String?
    var longitude : String?
    var address : String?
    var area : Areas?
    var owner : Owner?
    var schedule : [scheduleData]?
    var services : [ServicesData]?
    var packages : [packagesData]?
    var review : [reviewsData]?
    var review_count : Int?
    var review_average : String?
    var distance : QuantumValue?
    var images : [Images]?
   
}
class Reviews : Decodable {
    var data : [reviewsData]?
}
class Days : Decodable {
    var data : [scheduleData]?
}
class Day : Decodable {
    var data : scheduleData?
}
class scheduleData : Decodable {
    var id : Int?
    var from_inside : String?
    var to_inside : String?
    var from_outside : String?
    var to_outside : String?
    var to : String?
    var from : String?
    var day_of_week : Int?
    var date : String?

}
class packagesData : Decodable {
    var id : Int?
    var name : String?
    var price : String?
    var description : String?
    var short_description : String?
    var image : [Images]?
    var services : [ServicesData]?
    var type : QuantumValue?
    var type_label : String?
    var items : [ServicesData]?
}
struct  QuantumValue: Decodable {
    var IInt : Int?
    var SString : String?
    var ArString : [Any]?
    
    //    case int(Int), string(String)
    
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self.IInt = int
            return
        }
        
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self.SString = string
            return
        }
        if let array = try? decoder.singleValueContainer().decode([String].self) {
            self.ArString = array
            return
        }
        throw QuantumError.missingValue
    }
    
    enum QuantumError:Error {
        case missingValue
    }
}
class Reservation : Decodable {
    var data : ReservationData?
}
class MyReservation : Decodable {
    var data : [ReservationData]?
}
class ReservationData : Decodable {
    var id : Int?
    var status : String?
    var total_price : Int?
    var date : scheduleData?
    var packages : [packagesData]?
    var shop : ShopData?
    var service_items : CartContent?  //check here
}
class ReservationForTime : Decodable {
    var data : ReservationDataForTime?
}
class ReservationDataForTime : Decodable {
    var id : Int?
    var status : String?
    var total_price : Int?
    var date : scheduleData?
    var packages : [packagesData]?
    var shop : ShopData?
    var times : [String]?
}
class Owner : Decodable {
    var id : Int?
    var name : String?
    var mobile : String?
    var image : Images?
    var email : String?
    var type : QuantumValue?
}
class ServicesData : Decodable {
    var id : Int?
    var category_id : Int?
    var name : String?
    var category : categoryData?
    var image : Images?
    var price : String?
    var type : QuantumValue?
    var description : String?
    var amount_of_time : QuantumValue?
    var time:[String]?
    var employee_name: String?
    var employee_id : Int?
    var due_time:String?
    var assigned_employee_name:String?
    var assigned_employee_id: Int?
    var employees:[EmployeeData]?
    var service:SingleServicesData?
}
class SingleServicesData : Decodable {
    var id : Int?
    var name : String?
    var category : categoryData?
    var image : Images?
    var price : String?
    var type : QuantumValue?
    var description : String?
    var amount_of_time : Int?
    var time:String?
    var employee_name: String?
    var employee_id : Int?
    var due_time:String?
    var employees:[EmployeeData]?
}
class ServiceData : Decodable {
    var id : Int?
    var name : String?
    var category : categoryData?
    var image : Images?
    var price : String?
    var type : QuantumValue?
    var description : String?
    var amount_of_time : Int?
}
class Images : Decodable {
    var small : String?
    var medium : String?
    var large : String?
    var original : String?
}

class categoryData : Decodable {
    var id : Int?
    var name : String?
}

class Rating : Decodable {
    var rating_average : Int?
    var rating_count : Int?
}
class LinksInfo : Decodable {
    var add_to_favorite : hrefAndMethod?
    var add_to_cart : hrefAndMethod?
    var rate:hrefAndMethod?
    var remove_from_favorite:hrefAndMethod?
}
class hrefAndMethod : Decodable {
    var href : String?
    var method : String?
}
class Services : Decodable {
    var data :[ServicesData]?
    var links : ServicesLinks?
    var meta : Meta?
}
class ServiceCategories: Decodable {
    var data :[categoryData]?
    var links : ServicesLinks?
    var meta : Meta?

}
class ReviewsForShop: Decodable {
    var data :[reviewsData]?
    var links : ServicesLinks?
    var meta : Meta?
}
class reviewsData : Decodable {
    var id : Int?
    var name : String?
    var review : Int?
    var message : String?
    var created_at : created_atData?
}
class created_atData : Decodable {
    var date : String?
    var timezone_type : Int?
    var timezone : String?
}

class ServicesLinks : Decodable {
    var first : String?
    var last : String?
    var prev : String?
    var next : String?
}
class Meta : Decodable {
    var current_page : Int?
    var from : Int?
    var last_page : Int?
    var path : String?
    var per_page : Int?
    var to : Int?
    var total : Int?
    
}
