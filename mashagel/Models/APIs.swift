//
//  APIs.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

//
import Foundation

class APIs {
    static let Instance = APIs()
    private init() {}
    
    private let url = "http://mashaghel.elnooronlineworks.com"
    
    public func getHeader() -> [String: String]{
        let header = [
            "Accept" : "application/json" , "Authorization" : "Bearer \(userData.Instance.token ?? "")" ,
            "onesignal-player-id" : "",
            "Accept-Language":L102Language.currentAppleLanguage()
        ]
        return header
    }
    
    public func registeration() -> String{
        return url + "/api/auth/register"
    }
    
    public func login() -> String{
        return url + "/api/auth/login"
    }
    public func googleLogin() -> String{
        return url + "/api/auth/login/social"
    }

    public func showAreas() -> String{
        return url + "/api/areas"
    }
    public func AddEmployee(id : Int) -> String{
        return url + "/api/employees/\(id)"
    }
    public func ShopsSearch() -> String{
        return url + "/api/home"
    }
    public func getServiceCategories() -> String{
        return url + "/api/categories"
    }
    public func getServices()-> String{
        return url + "/api/services"
    }
    public func messagesToApplication() -> String{
        return url + "/api/messages"
    }
    public func showShop(id : Int) -> String{
        return url + "/api/shops/\(id)"
    }
    public func sendShopReview(id : Int) -> String{
        return url + "/api/reviews/\(id)"
    }
    public func UpdateShop(id : Int) -> String{
        return url + "/api/shops/\(id)"
    }
    public func ListReviews(id : Int) -> String{
        return url + "/api/reviews/\(id)"
    }
    public func Forgetpassword() -> String{
        return url + "/api/auth/password/forget"
    }
    public func showSchedule(id : Int) -> String{
        return url + "/api/shop/\(id)/schedule"
    }
    public func AddShopSchedule(id : Int) -> String{
        return url + "/api/shop/\(id)/schedule"
    }
    public func showShopExceptions(id : Int) -> String{
        return url + "/api/shop/\(id)/exceptions"
    }
    public func showShopEmployees(id : Int) -> String{
        return url + "/api/employees/\(id)"
    }
    public func AddShopException(id : Int) -> String{
        return url + "/api/shop/\(id)/exceptions"
    }
    public func updateShopException(id : Int) -> String{
        return url + "/api/shop/exceptions/\(id)"
    }
    public func CurrentPreparingReservation(id : Int) -> String{
        return url + "/api/reservations/preparer/\(id)"
    }
    public func CurrentReservation(id : Int) -> String{
        return url + "/api/reservations/preparer?reservation_id=\(id)"
    }
    public func listEmployee(id : Int) -> String{
        return url + "/api/employees/\(id)"
    }
    public func SendDateForCurrentReservation(id : Int) -> String{
        return url + "/api/reservations/preparer/date?reservation_id=\(id)"
    }
    public func AddPackage(id : Int , identifier:Int) -> String{
        return url + "/api/reservations/preparer/packages/\(id)?reservation_id=\(identifier)"
    }
    public func AddService(id : Int , identifier:Int , serviceType:String , quantity: Int) -> String{
        return url + "/api/reservations/preparer/services/\(id)?reservation_id=\(identifier)&service_type=\(serviceType)&quantity=\(quantity)"
    }
    public func AddEmployeeSchedule(id : Int) -> String{
        return url + "/api/employee/\(id)/schedule"
    }
    public func showEmployeeSchedule(id : Int) -> String{
        return url + "/api/employee/\(id)/schedule"
    }
    public func AddEmployeeException(id : Int) -> String{
        return url + "/api/employee/\(id)/exceptions"
    }
    public func showEmployeeExceptions(id : Int) -> String{
        return url + "/api/employee/\(id)/exceptions"
    }
    public func showShopReservation(id : Int) -> String{
        return url + "/api/reservations/\(id)"
    }
    public func showReservation() -> String{
        return url + "/api/reservations"
    }
    public func showMyReservations() -> String{
        return url + "/api/reservations"
    }
    public func RemoveService(id : Int , identifier:Int) -> String{
        return url + "/api/reservations/preparer/services/\(id)?reservation_id=\(identifier)"
    }
    public func RemovePackage(id : Int , identifier:Int) -> String{
        return url + "/api/reservations/preparer/packages/\(id)?reservation_id=\(identifier)"
    }
    public func setEmployeeId(id : Int , identifier:Int) -> String{
        return url + "/api/reservations/preparer/items/\(id)/employee?reservation_id=\(identifier)"
    }
    public func setDuetime(id : Int  , identifier:Int) -> String{
        return url + "/api/reservations/preparer/items/\(id)/due_time?reservation_id=\(identifier)"
    }
    public func ConfirmReservation( identifier:Int) -> String{
        return url + "/api/reservations/preparer/confirm?reservation_id=\(identifier)"
    }
//
    public func Settings() -> String{
        return url + "/api/settings"
    }
    public func contactMessages() -> String{
        return url + "/api/contact_messages"
    }
    public func listingCurrencies() -> String{
        return url + "/api/currencies"
    }
    public func listingCountries() -> String{
        return url + "/api/countries"
    }
    public func shoppingCartList() -> String{
        print(url + "/api/shopping_cart_products")
        return url + "/api/shopping_cart_products"
    }
    public func addProductToShoppingCart() -> String{
        return url + "/api/shopping_cart_products"
    }
    public func UpdateItemInShoppingCart(id : Int) -> String{
        print(url + "/api/shopping_cart_products/\(id)")
        return url + "/api/shopping_cart_products/\(id)"
    }
    public func DeleteItemInShoppingCart(id : Int) -> String{
        return url + "/api/shopping_cart_products/\(id)"
    }
    public func addCoupon() -> String{
        return url + "/api/add-coupon"
    }
    public func Checkout() -> String{
        return url + "/api/checkout"
    }
    public func MyAddresses() -> String{
        return url + "/api/addresses"
    }
    
    public func showFavorites() -> String{
        return url + "/api/favorites"
    }
    
    public func AddToFavorites(id : Int) -> String{
        print(url + "/api/products/\(id)/favorites")
        return url + "/api/products/\(id)/favorites"
    }
    
    
    public func ListMyOrders() -> String{
        return url + "/api/orders"
    }
    
    public func ShowOrder(id : Int) -> String{
        return url + "/api/orders/\(id)"
    }
    
}

