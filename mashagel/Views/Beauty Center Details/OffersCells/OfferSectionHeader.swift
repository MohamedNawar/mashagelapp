//
//  OfferSectionHeader.swift
//  mashagel
//
//  Created by ElNoorOnline on 1/6/19.
//  Copyright © 2019 MohamedHassanNawar. All rights reserved.
//

import UIKit

class OfferSectionHeader: UITableViewCell {

    @IBOutlet weak var category_name: UILabel!
    @IBOutlet weak var collapsable_img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure_cell(data : String) {
        
        
            category_name.text = data 

        
    }
    
}
