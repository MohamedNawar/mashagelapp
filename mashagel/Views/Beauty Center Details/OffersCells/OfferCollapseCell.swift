//
//  OfferCollapseCell.swift
//  mashagel
//
//  Created by ElNoorOnline on 1/6/19.
//  Copyright © 2019 MohamedHassanNawar. All rights reserved.
//

import UIKit
import SDWebImage


protocol TableViewCellDelegate : class {
     func didAddService(_ sender: OfferCollapseCell)
}

class OfferCollapseCell: UITableViewCell {

    @IBOutlet weak var service_image: UIImageView!
    
    @IBOutlet weak var service_name: UILabel!
    
    @IBOutlet weak var serviceTime_lbl: UILabel!
    
    @IBOutlet weak var servicePrice_lbl: UILabel!
    
    
    @IBOutlet weak var numOfPerson_lbl: UILabel!
    @IBOutlet weak var increase_btn: UIButton!
    
    
    weak var delegate: TableViewCellDelegate?
    
    @IBOutlet weak var addService_btn: btn!
    @IBAction func addService_btn(_ sender: UIButton) {
        delegate?.didAddService(self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     
    }
    
     func configure_cell(data : ServicesData) {
        
        if data != nil {
             service_name.text = data.name
         //   serviceTime_lbl.text = "\(data.amount_of_time?.IInt!)SR"
            servicePrice_lbl.text = data.price
            
        //    if let  imageIndex = data.image {
                
                service_image.sd_setImage(with: URL(string: "http://mashaghel.elnooronlineworks.com/media/2/conversions/medium.png"), completed: nil)
          //  }
            
        }
    }
    @IBAction func increase_btn(_ sender: Any) {
        
        let current_count = Int(numOfPerson_lbl.text!)
        if  current_count != 1{
            numOfPerson_lbl.text = "\(current_count! - 1 )"
        }
    }
    
    
    @IBAction func decrease_btn(_ sender: Any) {
        
        let current_count = Int(numOfPerson_lbl.text!)
     
       numOfPerson_lbl.text = "\(current_count! + 1 )"
        
    }
    
    
}
