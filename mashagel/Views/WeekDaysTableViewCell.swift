//
//  WeekDaysTableViewCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/7/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class WeekDaysTableViewCell: UITableViewCell {
var editWeekDayCallBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

 
    @IBAction func edit(_ sender: Any) {
        editWeekDayCallBack!()
    }
    
}
