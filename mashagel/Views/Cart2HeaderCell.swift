//
//  Cart2HeaderCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/10/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class Cart2HeaderCell: UITableViewCell {
 var DeleteServiceCallBack: (() -> Void)?
    @IBOutlet weak var packageName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func updateViews(Package: Package_item) {
        if L102Language.currentAppleLanguage() == "ar"{
            packageName.textAlignment = .right
        }else{
            packageName.textAlignment = .left

        }
        packageName.text = Package.name ?? ""
    }

    @IBAction func deletePackage(_ sender: Any) {
        DeleteServiceCallBack!()
    }
}
