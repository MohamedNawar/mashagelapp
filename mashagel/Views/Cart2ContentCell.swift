//
//  Cart2ContentCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/10/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class Cart2ContentCell: UITableViewCell ,UIPickerViewDataSource , UIPickerViewDelegate , UITextFieldDelegate {
    let employeePicker = UIPickerView()
    var words = [EmployeeData]()
    let timePicker = UIPickerView()
    var words2 = [Any]()
    var DeleteServiceCallBack: (() -> Void)?
    var addEmployeeServiceCallBack: ((Int) -> Void)?
    var addTimerServiceCallBack: ((String) -> Void)?
    var selectedItem = ""
     var selectedItem2 = ""
    var employeeId :Int = 0
    var reservation = Reservation()
    @IBOutlet weak var chooseTime: UITextField!
    @IBOutlet weak var chooseEmployee: UITextField!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var serviceDescription2: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        chooseEmployee.isEnabled = false
        chooseTime.isEnabled = false
        timePicker.backgroundColor = .white
        timePicker.delegate = self
        timePicker.dataSource = self
        employeePicker.delegate = self
        employeePicker.dataSource = self
        employeePicker.backgroundColor = .white
        employeePicker.showsSelectionIndicator = true
        timePicker.showsSelectionIndicator = true
        chooseEmployee.addPadding(UITextField.PaddingSide.left(20))
        chooseTime.addPadding(UITextField.PaddingSide.left(20))
        chooseEmployee.delegate = self
        chooseTime.delegate = self
        chooseTime.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        chooseEmployee.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
showEmployeePicker()
        showTimePicker()
        if L102Language.currentAppleLanguage() == "ar"{
            serviceName.textAlignment = .right
            serviceDescription.textAlignment = .right
            serviceDescription2.textAlignment = .right
        }
    }
    func updateViews(service: ServicesData) {
        serviceName.text = service.service?.name ?? ""
        servicePrice.text = "$\(service.service?.price ?? "")"
        serviceDescription2.text = service.service?.description ?? ""
        serviceDescription.text = "\(service.amount_of_time?.IInt ?? 0) minute"
        if let assignedEmployee = service.assigned_employee_id {
            for employee in service.employees ?? [EmployeeData]() {
                if employee.employee_id == assignedEmployee {
            chooseEmployee.text = employee.employee_name
                }
            }
        }
        if let assignedTime = service.due_time {
            chooseTime.text = assignedTime
            }
        words = service.employees ?? [EmployeeData]()
        if words.count <= 0 {
            chooseEmployee.isEnabled = false
            chooseTime.isEnabled = false

        }else{
            chooseEmployee.isEnabled = true
        }
        self.employeePicker.reloadInputViews()
    }
    func showTimePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseTime.inputView = timePicker
        chooseTime.inputAccessoryView = toolbar
    }
    @objc func donedatePicker(){
        if chooseTime.text == "" {
            if words2.count > 0 {
                chooseTime.text = words2[0]  as! String
            } else {
                self.endEditing(true)
            }
            addTimerServiceCallBack!(words2[0] as! String)
        }
       
        self.endEditing(true)
    }
    @objc func cancelDatePicker(){
        chooseTime.text = ""
        self.endEditing(true)
    }

    func showEmployeePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseEmployee.inputView = employeePicker
        chooseEmployee.inputAccessoryView = toolbar
    }
    @objc func cancel(){
        chooseEmployee.text = ""
        self.endEditing(true)
    }
    @objc func donePicker(){
        if chooseEmployee.text == "" {
            if words.count > 0 {
                chooseEmployee.text = words[0].employee_name ?? ""
                print(words[0].employee_name ?? "")
                employeeId = words[0].employee_id ?? 0
                words2 = words[0].times?.ArString ?? [String]()
                if words2.count > 0 {
                    chooseTime.isEnabled = true
                }
                addEmployeeServiceCallBack!(employeeId)
            }
        }
        addEmployeeServiceCallBack!(employeeId)
        self.endEditing(true)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
        if textField == chooseTime  {
            chooseTime.placeholder = ""
        }
        if textField == chooseEmployee {
            chooseEmployee.placeholder = ""
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            if textField == chooseTime  {
                chooseTime.placeholder = "Time"
            }
            if textField == chooseEmployee {
                chooseEmployee.placeholder = "Employee"
            }
    }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == employeePicker {
            print(words.count ?? 0)
            return words.count ?? 0
        }
        if pickerView == timePicker {
            print(words2.count ?? 0)
            return words2.count ?? 0
        }
        return 0
        }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == employeePicker {
            return "\(words[row].employee_name ?? "")"
            
        }
        if pickerView == timePicker {
            return "\(words2[row])"
            
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if pickerView == employeePicker {
            selectedItem = words[row].employee_name ?? ""
            employeeId = words[row].employee_id ?? 0
            words2 = words[row].times?.ArString ?? [Any]()
            if words2.count > 0 {
                chooseTime.isEnabled = true
            }
            chooseEmployee.text = selectedItem
        }
        if pickerView == timePicker {
            selectedItem2 = words2[row] as! String
            chooseTime.text = selectedItem2
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    @IBAction func deleteService(_ sender: Any) {
        DeleteServiceCallBack!()
    }
}
