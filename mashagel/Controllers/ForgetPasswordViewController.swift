//
//  ForgetPasswordViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/4/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class ForgetPasswordViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var secondTextField: DesignableTextField!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var firstLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        
        firstTextField.delegate = self
        secondTextField.delegate = self
        firstTextField.addPadding(UITextField.PaddingSide.left(20))
        secondTextField.addPadding(UITextField.PaddingSide.left(20))
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    
    func Postemail(){
        if self.firstTextField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        
        let header = APIs.Instance.getHeader()
        let par = [ "email": firstTextField.text!] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.Forgetpassword(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print("successsss")
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func send(_ sender: Any) {
        Postemail()
    }
}
