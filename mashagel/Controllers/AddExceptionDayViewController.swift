//
//  AddExceptionDayViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/7/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import CoreLocation
import FCAlertView

class AddExceptionDayViewController: UIViewController {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var to1: UILabel!
    @IBOutlet weak var from1: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var  fromTextField1: UITextField!
    @IBOutlet weak var  dateTextField: DesignableTextField!
    @IBOutlet weak var  hiddentextField: UITextField!
    @IBOutlet weak var toTextField1: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    var id = Int()
    var day = Int()
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var weekDays = Day()
    let datePicker = UIDatePicker()
    let fromPicker1 = UIDatePicker()
    let toPicker1 = UIDatePicker()
    let fromPicker = UIDatePicker()
    let toPicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Add Employee", comment: "إضافة موظف")
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        id = userData.Instance.userSelectedShopId as! Int
        toPicker.backgroundColor = .white
        fromPicker.backgroundColor = .white
        hiddentextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil);fromTextField1.addPadding(UITextField.PaddingSide.left(20))
        toTextField1.addPadding(UITextField.PaddingSide.left(20))
        fromTextField.addPadding(UITextField.PaddingSide.left(20))
        toTextField.addPadding(UITextField.PaddingSide.left(20))
        fromTextField1.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        toTextField1.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        fromTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        toTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        showToPicker()
        showFromPicker()
        showToPicker1()
        showFromPicker1()
        showDatePicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if L102Language.currentAppleLanguage() == "ar" {
           date.textAlignment = .right
            to1.textAlignment = .right
            from1.textAlignment = .right
            to.textAlignment = .right
            from.textAlignment = .right
           fromTextField1.textAlignment = .right
           dateTextField.textAlignment = .right
           hiddentextField.textAlignment = .right
            toTextField1.textAlignment = .right
            toTextField.textAlignment = .right
           fromTextField.textAlignment = .right
        }
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker2));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    @objc func donedatePicker2(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    func showFromPicker(){
        //Formate Date
        fromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fromTextField.inputAccessoryView = toolbar
        fromTextField.inputView = fromPicker
        
    }
    func showToPicker(){
        //Formate Date
        toPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker1));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        toTextField.inputAccessoryView = toolbar
        toTextField.inputView = toPicker
        
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fromTextField.text = formatter.string(from: fromPicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func donedatePicker1(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        toTextField.text = formatter.string(from: toPicker.date)
        self.view.endEditing(true)
    }
    func showFromPicker1(){
        //Formate Date
        fromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fromTextField1.inputAccessoryView = toolbar
        fromTextField1.inputView = fromPicker1
        
    }
    func showToPicker1(){
        //Formate Date
        toPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker111));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        toTextField1.inputAccessoryView = toolbar
        toTextField1.inputView = toPicker1
        
    }
    @objc func donedatePicker11(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fromTextField1.text = formatter.string(from: fromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker111(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        toTextField1.text = formatter.string(from: toPicker1.date)
        self.view.endEditing(true)
    }
    func AddOfficialHolidaysShopsData(id:Int){
        let par = ["date": dateTextField.text ?? "" ,"from_inside": fromTextField.text ?? "" , "to_inside": toTextField.text ?? "","from_outside": fromTextField1.text ?? "" , "to_outside": toTextField1.text ?? "" ] as! [String :Any]
        //        if WebsiteTxtField.text != "" {
        //            if let temp1 = WebsiteTxtField.text  {
        //                par.updateValue(temp1, forKey: "social_media[website]")
        //            }
        //        }
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddShopException(id: id) , method: .post, parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekDays = try JSONDecoder().decode(Day.self, from: response.data!)
                        print(self.weekDays)
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func add(_ sender: Any) {
        self.dismiss(animated: true) {
            self.AddOfficialHolidaysShopsData(id: self.id)

        }
    }
    
}
