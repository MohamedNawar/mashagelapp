//
//  OfficialHolidaysViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class OfficialHolidaysViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    var id = Int(){
        didSet{
            //            print(id)
            //            OfficialHolidaysShopsData(id: id)
        }
    }
    var currentDate = String()
    var weekDays = Days(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var currentDay = scheduleData()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDays.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfficialHolidaysCell", for: indexPath) as! OfficialHolidaysTableViewCell
        currentDay = (weekDays.data?[indexPath.row])!
        if let titleValue = weekDays.data?[indexPath.row].date{
            let title = cell.viewWithTag(1) as! UILabel
            title.text = titleValue
        }
        if let titleValue1 = weekDays.data?[indexPath.row] {
            let title = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if var from = titleValue1.from_inside{
                if var to = titleValue1.to_inside{
                    var toCount = to.count
                    to.remove(at: String.Index(encodedOffset: toCount))
                    to.remove(at: String.Index(encodedOffset: toCount-1))
                    to.remove(at: String.Index(encodedOffset: toCount-2))
                    var fromCount = from.count
                    from.remove(at: String.Index(encodedOffset: fromCount))
                    from.remove(at: String.Index(encodedOffset: fromCount-1))
                    from.remove(at: String.Index(encodedOffset: fromCount-2))
                    from.characters.removeLast()
                    to.characters.removeLast()
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text =
                        "من الساعة \(from) صباحاإلي الساعة \(to) ليلا داخل الصالون"                                  }else{
                        title.text =
                            NSLocalizedString("from \(from) to \(to) inside Salon", comment:   "من الساعة \(from) صباحاإلي الساعة \(to) ليلا داخل الصالون")                                  }
                }
            }
        }
        if let titleValue = weekDays.data?[indexPath.row] {
            let title = cell.viewWithTag(3) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if var from = titleValue.from_outside{
                if var to = titleValue.to_outside{
                    var toCount = to.count
                    to.remove(at: String.Index(encodedOffset: toCount))
                    to.remove(at: String.Index(encodedOffset: toCount-1))
                    to.remove(at: String.Index(encodedOffset: toCount-2))
                    var fromCount = from.count
                    from.remove(at: String.Index(encodedOffset: fromCount))
                    from.remove(at: String.Index(encodedOffset: fromCount-1))
                    from.remove(at: String.Index(encodedOffset: fromCount-2))
                    from.characters.removeLast()
                    to.characters.removeLast()
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون"
                        
                    }else{
                        title.text = NSLocalizedString("from \(from) to \(to) outside Salon", comment:   "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون")
                    }
                }
            }
        }
        cell.OfficialHolidaysCallBack = {
            self.mangeschedule()
            
        }
        return cell
    }
    private func mangeschedule(){
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "updateExceptionDayViewController") as! updateExceptionDayViewController
        navVC.exceptionId = currentDay.id!
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        OfficialHolidaysShopsData(id: currentID)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        id = userData.Instance.userSelectedShopId as! Int
        OfficialHolidaysShopsData(id: id)
        if L102Language.currentAppleLanguage() == "ar"{
            self.title = "أجزات رسمية"

            
        }else{
            self.title = NSLocalizedString("Official Holidays", comment:"أجزات رسمية")

        }
    }
    
    @IBAction func addExceptionDay(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExceptionDayViewController") as! AddExceptionDayViewController
        navVC.id = id
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
    func OfficialHolidaysShopsData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showShopExceptions(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekDays = try JSONDecoder().decode(Days.self, from: response.data!)
                        print(self.weekDays)
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
