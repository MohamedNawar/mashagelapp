//
//  ContactUsViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class ContactUsViewController:   UIViewController , UITextFieldDelegate ,  UITextViewDelegate {
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var textViewView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var message: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        self.title = NSLocalizedString("Contact Us", comment: "تواصل معنا")
        phoneTextField.delegate = self
        messageTextView.delegate = self
        nameTextField.delegate = self; phoneTextField.addPadding(UITextField.PaddingSide.left(20))
        nameTextField.addPadding(UITextField.PaddingSide.left(20))
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if L102Language.currentAppleLanguage() == "ar" {
            phoneTextField.textAlignment = .right
          nameTextField.textAlignment = .right
          messageTextView.textAlignment = .right
           name.textAlignment = .right
          phone.textAlignment = .right
            message.textAlignment = .right
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textViewView.borderWidth = 1
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        textViewView.borderWidth = 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    
    func PostMessage(){
        if  self.nameTextField.text == "" || self.phoneTextField.text == "" || self.messageTextView.text == ""  {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        let par = ["name": nameTextField.text!,  "mobile": phoneTextField.text!, "message": messageTextView.text] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.messagesToApplication(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        
                        
                        print("successsss")
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func sendMessage(_ sender: Any) {
        PostMessage()
    }
}
