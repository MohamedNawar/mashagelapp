//
//  filterViewController.swift
//  mashagel
//
//  Created by iMac on 10/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

    import UIKit
    import Alamofire
    import PKHUD
    import CoreLocation
    import FCAlertView
    class filterViewController:
    UIViewController , UITextFieldDelegate , UIPickerViewDelegate , UIPickerViewDataSource , UITextViewDelegate , CLLocationManagerDelegate{
        
        @IBOutlet weak var serviceLocationLabel: UILabel!
        @IBOutlet weak var districtionLabel: UILabel!
        @IBOutlet weak var cityLabel: UILabel!
        @IBOutlet weak var serviceType: UISegmentedControl!
        @IBOutlet weak var districtTextField: UITextField!
        @IBOutlet weak var cityTextField: UITextField!
        var request = "http://mashaghel.elnooronlineworks.com/api/home"
        var shopes = Shops()
        let locationManager = CLLocationManager()
        var coordinateValue = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        let cityPicker = UIPickerView()
        let areaPicker = UIPickerView()
        let datePicker = UIDatePicker()
        var selectedCity = CitiesData()
        var selectedArea = Areas()
        var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
        var selected = ""
        var words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
        var selected1 = ""
        var cityId :Int = 0
        var areaId :Int = 0
        var selectedCityIndex = 0
        var serType = "inside"
        override func viewDidLoad() {
            super.viewDidLoad()
            userData.Instance.fetchUser()
            self.title = NSLocalizedString("Search", comment:"ابحث")
            navigationItem.title = NSLocalizedString("Search", comment:"ابحث")
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            districtTextField.isEnabled = false; self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.clear]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
            userData.Instance.fetchUser()
            if L102Language.currentAppleLanguage() == "ar" {
                cityLabel.textAlignment = .right
                districtionLabel.textAlignment = .right
                serviceLocationLabel.textAlignment = .right
            }
            print(userData.Instance.token)
            cityPicker.delegate = self
            areaPicker.delegate = self
            cityPicker.dataSource = self
            cityPicker.backgroundColor = .white
            cityPicker.showsSelectionIndicator = true
            areaPicker.delegate = self
            areaPicker.dataSource = self
            areaPicker.backgroundColor = .white
            areaPicker.showsSelectionIndicator = true
            datePicker.backgroundColor = .white
            districtTextField.delegate = self
            cityTextField.delegate = self
            districtTextField.addPadding(UITextField.PaddingSide.left(20))
            cityTextField.addPadding(UITextField.PaddingSide.left(20))
            districtTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            loadCityyData()
            showCityPicker()
            showAreaPicker()
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.clear]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
            if L102Language.currentAppleLanguage() == "ar" {
                cityLabel.textAlignment = .right
                districtionLabel.textAlignment = .right
                serviceLocationLabel.textAlignment = .right
            }
        }
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            if  let coordinate = locationManager.location?.coordinate {
                coordinateValue = coordinate
                self.locationManager.stopUpdatingLocation()
            }else {
            }
        }
        func loadCityyData() {
            HUD.show(.progress, onView: self.view)
            CityMain.Instance.getCitiesServer(enterDoStuff: { () in
                self.words = CityMain.Instance.getCitiesNameArr()
                self.cityPicker.reloadAllComponents()
                HUD.hide(animated: true)
            })
        }
        func showAreaPicker(){
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker1));
            let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
            toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
            toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            districtTextField.inputView = areaPicker
            districtTextField.inputAccessoryView = toolbar
        }
        
        func showCityPicker(){
            
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
            let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
            toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
            toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cityTextField.inputView = cityPicker
            cityTextField.inputAccessoryView = toolbar
        }
        @objc func cancel1(){
            districtTextField.text = ""
            self.view.endEditing(true)
        }
        @objc func donePicker1(){
            if districtTextField.text == "" {
                if words1.count > 0 {
                    districtTextField.text = words1[0] ?? ""
                    selectedArea = selectedCity.areas?[0] ?? Areas()
                    areaId = selectedArea.id ?? 0
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            self.view.endEditing(true)
        }
        
        @objc func cancel(){
            cityTextField.text = ""
            self.view.endEditing(true)
        }
        @objc func donePicker(){
            districtTextField.text = ""
            districtTextField.isEnabled = false
            districtTextField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            if cityTextField.text == "" {
                if words.count > 0 {
                    cityTextField.text = words[0]
                    selectedCity = CityMain.Instance.cityData.data?[0] ?? CitiesData()
                    cityId = selectedCity.id ?? 0
                    words1 = selectedCity.getAreasName()
                    self.areaPicker.reloadAllComponents()
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            self.view.endEditing(true)
            districtTextField.isEnabled = true
            self.areaPicker.reloadAllComponents()
        }
        func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            if textField.text == "" {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            }
        }
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if pickerView == cityPicker {
                return words.count
            }
            if pickerView == areaPicker {
                return words1.count
            }
            
            return 0
        }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if pickerView == cityPicker {
                return "\(words[row])"
                
            }
            if pickerView == areaPicker {
                return "\(words1[row])"
                
            }
            
            return ""
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView == cityPicker {
                selected = words[row]
                selectedCity = CityMain.Instance.cityData.data?[row] ?? CitiesData()
                selectedCityIndex = row
                words1 = selectedCity.getAreasName()
                cityId = selectedCity.id ?? 0
                cityTextField.text = selected
                areaPicker.reloadAllComponents()
            }
            if pickerView == areaPicker {
                selected1 = words1[row]
                selectedArea = selectedCity.areas?[row] ?? Areas()
                areaId = selectedArea.id ?? 0
                districtTextField.text = selected1
            }
            
        }
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        @IBAction func search(_ sender: Any) {
                if self.serviceType.selectedSegmentIndex == 1 {
                    self.serType = "outside"
                }else {
                    self.serType = "inside"
                }
                if self.cityTextField.text != "" {
                    
                    self.request =  "http://mashaghel.elnooronlineworks.com/api/home?area=\(self.areaId)?city=\(self.cityId)?latitude=\(self.coordinateValue.latitude)?longitude=\(self.coordinateValue.longitude)?type=\(self.serType)"
                    print(self.request)
                }
                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AllBeautyCenterViewController") as! AllBeautyCenterViewController
                navVC.url = self.request
                self.navigationController?.pushViewController(navVC, animated: true)
            }
        
}
