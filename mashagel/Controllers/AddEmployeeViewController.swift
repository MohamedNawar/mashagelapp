//
//  AddEmployeeViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

class AddEmployeeViewController: UIViewController , UIPickerViewDataSource , UIPickerViewDelegate , UITextFieldDelegate {
    let cityPicker = UIPickerView()
    let areaPicker = UIPickerView()
    let servicesPicker = UIPickerView()
    var selectedCity = CitiesData()
    var employee = employeeData()
    var selectedArea = Areas()
    var par = [String:Any]()
    var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    var words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
    var selected1 = ""
    var words2 = [ServicesData](){
        didSet{
            print(words2)
            servicesPicker.reloadAllComponents()
        }
    }
    var selected2 = ""
    var shopData = Shop(){
        didSet{
            words2 = shopData.data?.services ?? [ServicesData]()
            
        }
    }
    var cityId :Int = 0
    var areaId :Int = 0
    var serviceId:Int = 0
    var selectedCityIndex = 0
    var selectedService = [Int]()
    @IBOutlet weak var addEmployeeServices: UITextField!
    @IBOutlet weak var confirmPassword: DesignableTextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        phoneTextField.delegate = self
        emailTextField.delegate = self
        nameTextField.delegate = self
        idTextField.delegate = self
        confirmPassword.delegate = self
        addEmployeeServices.delegate = self
        passwordTextField.delegate = self
        cityPicker.delegate = self
        areaPicker.delegate = self
        cityPicker.dataSource = self
        cityPicker.backgroundColor = .white
        cityPicker.showsSelectionIndicator = true
        servicesPicker.delegate = self
        servicesPicker.dataSource = self
        servicesPicker.backgroundColor = .white
        servicesPicker.showsSelectionIndicator = true
        areaPicker.delegate = self
        areaPicker.dataSource = self
        areaPicker.backgroundColor = .white
        areaPicker.showsSelectionIndicator = true
        districtTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil);
        addEmployeeServices.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil);
        confirmPassword.addPadding(UITextField.PaddingSide.left(20))
        addEmployeeServices.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        nameTextField.addPadding(UITextField.PaddingSide.left(20))
        idTextField.addPadding(UITextField.PaddingSide.left(20))
        passwordTextField.addPadding(UITextField.PaddingSide.left(20))
        districtTextField.addPadding(UITextField.PaddingSide.left(20))
        cityTextField.addPadding(UITextField.PaddingSide.left(20))
        loadCityyData()
        showCityPicker()
        showAreaPicker()
        addServicesPicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if L102Language.currentAppleLanguage() == "ar" {
            addEmployeeServices.textAlignment = .right
            confirmPassword.textAlignment = .right
            cityTextField.textAlignment = .right
           districtTextField.textAlignment = .right
           phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
             idTextField.textAlignment = .right
             emailTextField.textAlignment = .right
             nameTextField.textAlignment = .right
        }
        userData.Instance.fetchUser()
        print("eeeeeee")
        let currentID = userData.Instance.userSelectedShopId as! Int
        print(userData.Instance.userSelectedShopId)
        loadShopData(id: currentID)
        words2 = shopData.data?.services ?? [ServicesData]()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    func loadCityyData() {
        HUD.show(.progress, onView: self.view)
        CityMain.Instance.getCitiesServer(enterDoStuff: { () in
            self.words = CityMain.Instance.getCitiesNameArr()
            self.cityPicker.reloadAllComponents()
            HUD.hide(animated: true)
        })
    }
    func showAreaPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title:NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        districtTextField.inputView = areaPicker
        districtTextField.inputAccessoryView = toolbar
    }
    
    func showCityPicker(){
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTextField.inputView = cityPicker
        cityTextField.inputAccessoryView = toolbar
    }
    func addServicesPicker(){
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker2));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel2))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        addEmployeeServices.inputView = servicesPicker
        addEmployeeServices.inputAccessoryView = toolbar
    }
    @objc func cancel2(){
        addEmployeeServices.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker2(){
        if addEmployeeServices.text == "" {
            if words2.count > 0 {
                addEmployeeServices.text = words2[0].name ?? ""
                serviceId = words2[0].id ?? 0
                selectedService.append(serviceId)
            }
        }
        selectedService.append(serviceId)
        self.view.endEditing(true)
    }
    @objc func cancel1(){
        districtTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker1(){
        if districtTextField.text == "" {
            if words1.count > 0 {
                districtTextField.text = words1[0] ?? ""
                selectedArea = selectedCity.areas?[0] ?? Areas()
                areaId = selectedArea.id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    
    @objc func cancel(){
        cityTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        districtTextField.text = ""
        districtTextField.isEnabled = false
        districtTextField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        if cityTextField.text == "" {
            if words.count > 0 {
                cityTextField.text = words[0]
                selectedCity = CityMain.Instance.cityData.data?[0] ?? CitiesData()
                cityId = selectedCity.id ?? 0
                words1 = selectedCity.getAreasName()
                self.areaPicker.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        districtTextField.isEnabled = true
        self.areaPicker.reloadAllComponents()
        self.servicesPicker.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityPicker {
            return words.count
        }
        if pickerView == areaPicker {
            return words1.count
        }
        if pickerView == servicesPicker {
            return words2.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPicker {
            return "\(words[row])"
        }
        if pickerView == areaPicker {
            return "\(words1[row])"
        }
        if pickerView == servicesPicker {
            var string = words2[row].name as! String
            print(string)
            return "\(string)"
            
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == cityPicker {
            selected = words[row]
            selectedCity = CityMain.Instance.cityData.data?[row] ?? CitiesData()
            selectedCityIndex = row
            words1 = selectedCity.getAreasName()
            cityId = selectedCity.id ?? 0
            cityTextField.text = selected
            areaPicker.reloadAllComponents()
        }
        if pickerView == areaPicker {
            selected1 = words1[row]
            selectedArea = selectedCity.areas?[row] ?? Areas()
            areaId = selectedArea.id ?? 0
            districtTextField.text = selected1
        }
        if pickerView == servicesPicker {
            selected2 = words2[row].name ?? ""
            print(selected2)
            serviceId = words2[row].id ?? 0
            addEmployeeServices.text = selected2
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    @IBAction func EditBtn(_ sender: Any) {
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        Add(id: currentID)
        self.dismiss(animated: true, completion: nil)
    }
    
    func Add(id:Int){
        if self.emailTextField.text == "" || self.nameTextField.text == "" || self.phoneTextField.text == "" || self.passwordTextField.text == "" || self.idTextField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        if self.passwordTextField.text != self.confirmPassword.text {
            HUD.flash(.label("Your Password Not Identical"), delay: 1.0)
            
            return
        }
        let header = APIs.Instance.getHeader()
        par.updateValue(nameTextField.text!, forKey: "name")
        par.updateValue(emailTextField.text!, forKey: "email")
        par.updateValue(phoneTextField.text!, forKey: "mobile")
        par.updateValue(idTextField.text!, forKey: "personal_id")
        par.updateValue(passwordTextField.text!, forKey: "password")
        par.updateValue(areaId, forKey: "area_id")
        par.updateValue(confirmPassword.text!, forKey: "password_confirmation")
        for i in 0..<selectedService.count {
            par.updateValue(selectedService[i], forKey: "services[\(i)]")
        }
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddEmployee(id: id), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.employee = try JSONDecoder().decode(employeeData.self, from: response.data!)
                        self.words2 = self.shopData.data?.services ?? [ServicesData]()
                        print(self.words2)
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                break
            }
        }
        
    }
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        
                        
                        print(self.shopData)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
}

