//
//  CategoryViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//


import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class CategoryViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate  {
    var shopData = Shop(){
        didSet{
            if let  imageIndex = shopData.data {
                let image = CategoryCollectionView.viewWithTag(11) as! UIImageView
                let  imageIndexString = imageIndex.images?.first!.large as! String
                image.sd_setImage(with: URL(string: imageIndexString), completed: nil)
                print(imageIndexString)
            }
        }
    }
    var id = Int() {
        didSet{
            //            loadShopData(id:id)
                       print(id)
        }
    }
    var imagesArray = [#imageLiteral(resourceName: "img36"),#imageLiteral(resourceName: "img35"),#imageLiteral(resourceName: "img38"),#imageLiteral(resourceName: "img37")]
    var nameArray = [NSLocalizedString("Working hours", comment: "أوقات الدوام"),
                     NSLocalizedString("ِEmployees", comment: "الموظفين"),
                     NSLocalizedString("Received Orders", comment: "الطلبات الواردة"),
                     NSLocalizedString("Center Data", comment: "بيانات السنتر")]
    
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        loadShopData(id: currentID)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = shopData.data?.name ?? ""
        if L102Language.currentAppleLanguage() == "ar"{
        nameArray = ["أوقات الدوام",
                            "الموظفين",
                              "الطلبات الواردة",
                            "بيانات السنتر"]
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "TimeOfWorksTabeViewController") as! TimeOfWorksTabeViewController
            self.navigationController?.pushViewController(navVC, animated: true)
            
        case 1:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ShowAllEmployeeForShopViewController") as! ShowAllEmployeeForShopViewController
            self.navigationController?.pushViewController(navVC, animated: true)
            
        case 2:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ShopReservation11ViewController") as! ShopReservation11ViewController
            self.navigationController?.pushViewController(navVC, animated: true)
            
        case 3:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "AbouteShopAndEditViewController") as! AbouteShopAndEditViewController
            self.navigationController?.pushViewController(navVC, animated: true)
            
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CategoryCollectionView {
            return 4
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath)
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
        image.image = imagesArray[indexPath.row]
        lbl.text = nameArray[indexPath.row]
        
        return cell
    }
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        
                        
                        print(self.shopData)
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var sectionHeaderView = UICollectionReusableView()
        if collectionView == self.CategoryCollectionView {
            switch kind {
                
            case UICollectionElementKindSectionHeader:
                sectionHeaderView = CategoryCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CategoryCellHeader", for: indexPath)
               
                return sectionHeaderView
                collectionView.reloadData()
                
            default:
                assert(false, "Unexpected element kind")
            }
            
        }
        
        
        return sectionHeaderView
        
    }
    @IBAction func changeLanguage(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "ar"{
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        };
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarControllerViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
}
extension CategoryViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.CategoryCollectionView.frame.width / 2)  , height: (self.CategoryCollectionView.frame.width / 2))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}





