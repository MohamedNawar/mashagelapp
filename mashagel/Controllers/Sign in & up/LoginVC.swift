//
//  LoginVC.swift
//  mashagel
//
//  Created by ElNoorOnline on 12/31/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit


// Match the ObjC symbol name inside Main.storyboard.
@objc(LoginVC)
class LoginVC: UIViewController, UITextFieldDelegate ,GIDSignInUIDelegate {


//, FBSDKLoginButtonDelegate{
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//
//    }
//
//    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
//
//    }
//

    
    @IBOutlet weak var email_txtField: UITextField!
    @IBOutlet weak var password_txtField: UITextField!
    
    var idToken:String = ""
    var social_media_login:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Swift
//        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
//            if (session != nil) {
//                print("signed in as \(session!.userName)");
//            } else {
//                print("error: \(error!.localizedDescription)");
//            }
//        })
//        logInButton.center = self.view.center
//        self.view.addSubview(logInButton)
        
        email_txtField.delegate = self
        password_txtField.delegate = self
        
        
        /*  google login */
        
        //adding the delegates
        GIDSignIn.sharedInstance().uiDelegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                                  object: nil)
    }
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                //        self.statusText.text = userInfo["statusText"]!
                idToken = userInfo["statusText"]!
                
                print("-----\(idToken)")
                
                if idToken != "" {
                    call_api(googleLogin: true)
                }
                //
                
            }
        }
    }
    
    /* sign_up */
    @IBAction func sign_up(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    /* login ==> phone & passsword*/
    @IBAction func login_btn(_ sender: Any) {
        
        if email_txtField.text?.isEmpty == false && password_txtField.text?.isEmpty == false {
            
            call_api(googleLogin: false)
        }else{
            
        }
        
    }
    
    /* signin later */
    @IBAction func signIn_later(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        self.dismiss(animated: true, completion: nil)
        goToMainScreen()
        
    }
    
    /* login ==> faceook*/
    @IBAction func faceBookLogin_btn(_ sender: Any) {
        
        let fbLoginManger: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManger.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
          
            
            if (error == nil){
                let fBLoginresult :FBSDKLoginManagerLoginResult = result!
                if fBLoginresult.grantedPermissions != nil {
                    if(fBLoginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                    }
                }
            }else{
                print(error)
            }
            
             
        }
        
     
    }
    
    /* login ==> faceook*/
    @IBAction func TwitterLogin_btn(_ sender: Any) {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if(session != nil){
                print("done")
            }else{
                print("no")
            }
        }
      
    }
    
    /* login ==> Google */
    @IBAction func googleLogin_btn(_ sender: Any) {
        
      GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK: - faceBook login
    
    func loginBtnDidLogout(){
        
    }
    
    func getFBUserData(){
        if (FBSDKAccessToken.current() != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, picture.type(large), email"])?.start(completionHandler: { (connection, result, error) in
                
                if (error == nil){
                    let faceDict = result as! [String:AnyObject]
                    print(faceDict)
                }
            })
        }
    }
    
    // MARK: call api
    func call_api(googleLogin: Bool){
        
        HUD.show(.progress, onView: self.view)
        
        
        var param:[String:Any] = [:]
        var request = ""
        if googleLogin  {
            

            social_media_login = "google"
            
            param =  [ "access_token" : idToken,
                        "driver" : social_media_login,
                        "onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? ""
            ]
            request = APIs.Instance.googleLogin()
            
        }else {
            param = [ "email" : "+966\(email_txtField.text!)",
                      "password" : password_txtField.text!,
                      "onesignal-player-id": UserDefaults.standard.string(forKey: "onesignalid") ?? ""
            ]
           
            //  "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid")
            request = APIs.Instance.login()
        }
        
        
        print(param)
        callApi(vc:self, withURL: request, method: .post, headers: APIs.Instance.getHeader(), Parameter: param) { (result) in
            
            
            HUD.hide()
            
            print("----done ----")
            switch result {
            case .success(let data):
                
                do {
                   // let user_data :userData   = try JSONDecoder().decode(userData.self, from: data)
                    userData.Instance.saveUser(data: data)
                    userData.Instance.fetchUser()
                    print("successsss")
                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                    
                    
                    if userData.Instance.data?.type == 2 {
                         self.goToAdmin()
                    }else{
                         self.goToMainScreen()
                        
                    }
                   
                }catch{
                    HUD.flash(.label("Error Try Again"), delay: 1.0)
                }
                HUD.flash(.success, delay: 1.0)

            default:
                print("-- error")
            }
            
        }
    }
    
    func goToMainScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarControllerViewController") as! TabBarControllerViewController

        self.present(vc, animated: true, completion: nil)
    }
    
    func goToAdmin()  {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AllBeautyCenterForAdminViewController")
        let vcc = UINavigationController(rootViewController: vc)
        
        self.present(vcc, animated: true, completion: nil)
        
        
    }
 
}
