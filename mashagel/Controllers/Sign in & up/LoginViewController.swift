//
//  LoginViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

class LoginViewController: UIViewController , UITextFieldDelegate {
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var backTo = String()
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordlTextField: DesignableTextField!
    @IBOutlet weak var emailTextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordlTextField.delegate = self
        emailTextField.delegate = self
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        passwordlTextField.addPadding(UITextField.PaddingSide.left(20))
        if L102Language.currentAppleLanguage() == "ar" {
            self.title =
        "تسجيل الدخول"
        }else{
            self.title = NSLocalizedString("Sign In", comment: "تسجيل الدخول")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
        if textField == emailTextField {
            emailLbl.isHidden = false
            emailTextField.placeholder = ""
        }
        if textField == passwordlTextField {
            passwordLbl.isHidden = false
            passwordlTextField.placeholder = ""
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            if textField == emailTextField {
                emailLbl.isHidden = true
            }
            if textField == passwordlTextField {
                passwordLbl.isHidden = true
            }
        }
    }
    @IBAction func LoginBtn(_ sender: Any) {
        login()
    }
    
    func login(){
        if emailTextField.text!.isEmpty == false && passwordlTextField.text!.isEmpty == false {
//            if !(emailTextField.text?.isValidEmail())!{
//                makeDoneAlert(title: "Error", SubTitle: "Check if your Email is Valid", Image: #imageLiteral(resourceName: "img34"))
//                return
//            }
            let header = APIs.Instance.getHeader()
            let par = ["email": emailTextField.text!,
                       "password": passwordlTextField.text!,
                        "onesignal-player-id": UserDefaults.standard.string(forKey: "onesignalid") ?? ""
                       ]
            
           
            print(par)
            
            HUD.show(.progress)
            Alamofire.request(APIs.Instance.login() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            userData.Instance.saveUser(data: response.data!)
                            userData.Instance.fetchUser()
                            print("successsss")
//                            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
//                            self.dismiss(animated: true, completion: nil)
                            self.goToAdmin()
                        }catch{
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                        HUD.flash(.success, delay: 1.0)
                        
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }else{
            makeDoneAlert(title: "حقل فارغ", SubTitle: "من فضلك تأكد من إدخال جميع البيانات", Image: #imageLiteral(resourceName: "img34"))
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم") , andButtons: nil)
    }
    
    func goToAdmin()  {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AllBeautyCenterForAdminViewController") as!  AllBeautyCenterForAdminViewController
        let vcc = UINavigationController(rootViewController: vc)
        
        self.present(vcc, animated: true, completion: nil)
        
    }
}



extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: length))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    public var length: Int {
        return self.characters.count
    }
}



