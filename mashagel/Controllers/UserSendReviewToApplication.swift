//
//  UserSendReviewToApplication.swift
//  mashagel
//
//  Created by iMac on 10/14/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class UserSendReviewToApplication: UIViewController , UITextFieldDelegate ,  UITextViewDelegate {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var textViewView: UIView!
    @IBOutlet weak var rattingView: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        rattingView.value = CGFloat(0)
        messageTextView.delegate = self
        nameTextField.delegate = self;
        nameTextField.addPadding(UITextField.PaddingSide.left(20))
        self.title = NSLocalizedString("Send Review", comment:"أرسال تقييم")
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textViewView.borderWidth = 1
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        textViewView.borderWidth = 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    func PostMessage(id:Int){
        if  self.nameTextField.text == "" ||  self.messageTextView.text == ""  {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        let par = ["name": nameTextField.text!,  "review":"\(rattingView.value)", "message": messageTextView.text] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.sendShopReview(id: id), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        
                        
                        print("successsss")
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func sendMessage(_ sender: Any) {
        let  id = userData.Instance.userSelectedShopId as! Int
        PostMessage(id:id)
    }
}
