//
//  ShowServiceImagesViewController.swift
//  mashagel
//
//  Created by iMac on 11/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Auk
import moa
class ShowServiceImagesViewController: UIViewController {
    var images = [String](){
        didSet{
            setScrollView()
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
      
        scrollView.auk.settings.contentMode = .scaleToFill
    }
    private func setScrollView(){
        for image in images {
            scrollView.auk.show(url:image )
            
            
        }
    }
        @IBAction func dismiss(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
}

