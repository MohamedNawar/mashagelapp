//
//  AddScheduleViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/7/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
class AddScheduleViewController: UIViewController {
    let saturdayFromPicker = UIDatePicker()
    let saturdayToPicker = UIDatePicker()
    let sundayFromPicker = UIDatePicker()
    let sundayToPicker = UIDatePicker()
    let mondayFromPicker = UIDatePicker()
    let mondayToPicker = UIDatePicker()
    let tuesdayFromPicker = UIDatePicker()
    let tuesdayToPicker = UIDatePicker()
    let wednesdayFromPicker = UIDatePicker()
    let wednesdayToPicker = UIDatePicker()
    let thursdayFromPicker = UIDatePicker()
    let thursdayToPicker = UIDatePicker()
    let fridayFromPicker = UIDatePicker()
    let fridayToPicker = UIDatePicker()
    let saturdayFromPicker1 = UIDatePicker()
    let saturdayToPicker1 = UIDatePicker()
    let sundayFromPicker1 = UIDatePicker()
    let sundayToPicker1 = UIDatePicker()
    let mondayFromPicker1 = UIDatePicker()
    let mondayToPicker1 = UIDatePicker()
    let tuesdayFromPicker1 = UIDatePicker()
    let tuesdayToPicker1 = UIDatePicker()
    let wednesdayFromPicker1 = UIDatePicker()
    let wednesdayToPicker1 = UIDatePicker()
    let thursdayFromPicker1 = UIDatePicker()
    let thursdayToPicker1 = UIDatePicker()
    let fridayFromPicker1 = UIDatePicker()
    let fridayToPicker1 = UIDatePicker()
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var fridayTo1: HoshiTextField!
    @IBOutlet weak var fridayFrom1: HoshiTextField!
    @IBOutlet weak var thursdayTo1: HoshiTextField!
    @IBOutlet weak var thursdayFrom1: HoshiTextField!
    @IBOutlet weak var wednesdayTo1: HoshiTextField!
    @IBOutlet weak var wednesdayFrom1: HoshiTextField!
    @IBOutlet weak var tuesdayTo1: HoshiTextField!
    @IBOutlet weak var tuesdayFrom1: HoshiTextField!
    @IBOutlet weak var mondayTo1: HoshiTextField!
    @IBOutlet weak var mondayFrom1: HoshiTextField!
    @IBOutlet weak var sundayTo1: HoshiTextField!
    @IBOutlet weak var sundayFrom1: HoshiTextField!
    @IBOutlet weak var saturdayTo1: HoshiTextField!
    @IBOutlet weak var saturdayFrom1: HoshiTextField!
    @IBOutlet weak var fridayTo: HoshiTextField!
    @IBOutlet weak var fridayFrom: HoshiTextField!
    @IBOutlet weak var thursdayTo: HoshiTextField!
    @IBOutlet weak var thursdayFrom: HoshiTextField!
    @IBOutlet weak var wednesdayTo: HoshiTextField!
    @IBOutlet weak var wednesdayFrom: HoshiTextField!
    @IBOutlet weak var tuesdayTo: HoshiTextField!
    @IBOutlet weak var tuesdayFrom: HoshiTextField!
    @IBOutlet weak var mondayTo: HoshiTextField!
    @IBOutlet weak var mondayFrom: HoshiTextField!
    @IBOutlet weak var sundayTo: HoshiTextField!
    @IBOutlet weak var sundayFrom: HoshiTextField!
    @IBOutlet weak var saturdayTo: HoshiTextField!
    @IBOutlet weak var saturdayFrom: HoshiTextField!
    var id = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        id = userData.Instance.userSelectedShopId as! Int
        self.title = NSLocalizedString("Add shop schedule", comment: "أضافة جدول المحل")
        // Do any additional setup after loading the view.
        saturdayFromPicker.backgroundColor = .white
        saturdayToPicker.backgroundColor = .white
        sundayFromPicker.backgroundColor = .white
        sundayToPicker.backgroundColor = .white
        mondayFromPicker.backgroundColor = .white
        mondayToPicker.backgroundColor = .white
        tuesdayFromPicker.backgroundColor = .white
        tuesdayToPicker.backgroundColor = .white
        wednesdayFromPicker.backgroundColor = .white
        wednesdayToPicker.backgroundColor = .white
        thursdayFromPicker.backgroundColor = .white
        thursdayToPicker.backgroundColor = .white
        fridayFromPicker.backgroundColor = .white
        fridayToPicker1.backgroundColor = .white
        saturdayFromPicker1.backgroundColor = .white
        saturdayToPicker1.backgroundColor = .white
        sundayFromPicker1.backgroundColor = .white
        sundayToPicker1.backgroundColor = .white
        mondayFromPicker1.backgroundColor = .white
        mondayToPicker1.backgroundColor = .white
        tuesdayFromPicker1.backgroundColor = .white
        tuesdayToPicker1.backgroundColor = .white
        wednesdayFromPicker1.backgroundColor = .white
        wednesdayToPicker1.backgroundColor = .white
        thursdayFromPicker1.backgroundColor = .white
        thursdayToPicker1.backgroundColor = .white
        fridayFromPicker1.backgroundColor = .white
        fridayToPicker1.backgroundColor = .white
        
        showsaturdayFromPicker()
        showsaturdayToPicker()
        showSundayFromPicker()
        showsundayToPicker()
        showMondayFromPicker()
        showMondayToPicker()
        showTuesdayFromPicker()
        showTuesdayToPicker()
        showWednesdayFromPicker()
        showWednesdayToPicker()
        showThursdayFromPicker()
        showThursdayToPicker()
        showFridayFromPicker()
        showFridayToPicker()
        
        showsaturdayFromPicker1()
        showsaturdayToPicker1()
        showSundayFromPicker1()
        showsundayToPicker1()
        showMondayFromPicker1()
        showMondayToPicker1()
        showTuesdayFromPicker1()
        showTuesdayToPicker1()
        showWednesdayFromPicker1()
        showWednesdayToPicker1()
        showThursdayFromPicker1()
        showThursdayToPicker1()
        showFridayFromPicker1()
        showFridayToPicker1()
    }
    func showsaturdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        saturdayFrom.inputAccessoryView = toolbar
        saturdayFrom.inputView = saturdayFromPicker
        
    }
    func showsaturdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker1));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        saturdayTo.inputAccessoryView = toolbar
        saturdayTo.inputView = saturdayToPicker
        
    }
    func showsaturdayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        saturdayFrom1.inputAccessoryView = toolbar
        saturdayFrom1.inputView = saturdayFromPicker1
        
    }
    func showsaturdayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker1));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        saturdayTo1.inputAccessoryView = toolbar
        saturdayTo1.inputView = saturdayToPicker1
        
    }
    //
    func showSundayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker2));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        sundayFrom.inputAccessoryView = toolbar
        sundayFrom.inputView = sundayFromPicker
        
    }
    func showsundayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker3));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        sundayTo.inputAccessoryView = toolbar
        sundayTo.inputView = sundayToPicker
        
    }
    func showSundayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedate1Picker2));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        sundayFrom1.inputAccessoryView = toolbar
        sundayFrom1.inputView = sundayFromPicker1
        
    }
    func showsundayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker3));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        sundayTo1.inputAccessoryView = toolbar
        sundayTo1.inputView = sundayToPicker1
        
    }
    //
    func showMondayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker4));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        mondayFrom.inputAccessoryView = toolbar
        mondayFrom.inputView = mondayFromPicker
        
    }
    func showMondayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker5));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        mondayTo.inputAccessoryView = toolbar
        mondayTo.inputView = mondayToPicker
        
    }
    func showMondayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker4));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        mondayFrom1.inputAccessoryView = toolbar
        mondayFrom1.inputView = mondayFromPicker1
        
    }
    func showMondayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker5));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        mondayTo1.inputAccessoryView = toolbar
        mondayTo1.inputView = mondayToPicker1
        
    }
    //
    func showTuesdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker6));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tuesdayFrom.inputAccessoryView = toolbar
        tuesdayFrom.inputView = tuesdayFromPicker
        
    }
    func showTuesdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم") , style: .plain, target: self, action: #selector(donedatePicker7));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tuesdayTo.inputAccessoryView = toolbar
        tuesdayTo.inputView = tuesdayToPicker
        
    }
    func showTuesdayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker6));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tuesdayFrom1.inputAccessoryView = toolbar
        tuesdayFrom1.inputView = tuesdayFromPicker1
        
    }
    func showTuesdayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker7));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tuesdayTo1.inputAccessoryView = toolbar
        tuesdayTo1.inputView = tuesdayToPicker1
        
    }
    //
    func showWednesdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker8));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        wednesdayFrom.inputAccessoryView = toolbar
        wednesdayFrom.inputView = wednesdayFromPicker
        
    }
    func showWednesdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker9));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        wednesdayTo.inputAccessoryView = toolbar
        wednesdayTo.inputView = wednesdayToPicker
        
    }
    func showWednesdayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedate1Picker8));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        wednesdayFrom1.inputAccessoryView = toolbar
        wednesdayFrom1.inputView = wednesdayFromPicker1
        
    }
    func showWednesdayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString(NSLocalizedString("Done", comment: "تم"), comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker9));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        wednesdayTo1.inputAccessoryView = toolbar
        wednesdayTo1.inputView = wednesdayToPicker1
        
    }
    //
    func showThursdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker10));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        thursdayFrom.inputAccessoryView = toolbar
        thursdayFrom.inputView = thursdayFromPicker
        
    }
    func showThursdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        thursdayTo.inputAccessoryView = toolbar
        thursdayTo.inputView = thursdayToPicker
        
    }
    func showThursdayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedate1Picker10));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        thursdayFrom1.inputAccessoryView = toolbar
        thursdayFrom1.inputView = thursdayFromPicker1
        
    }
    func showThursdayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        thursdayTo1.inputAccessoryView = toolbar
        thursdayTo1.inputView = thursdayToPicker1
        
    }
    //
    func showFridayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker12));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fridayFrom.inputAccessoryView = toolbar
        fridayFrom.inputView = fridayFromPicker
        
    }
    func showFridayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker13));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fridayTo.inputAccessoryView = toolbar
        fridayTo.inputView = fridayToPicker
        
    }
    func showFridayFromPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedate1Picker12));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fridayFrom1.inputAccessoryView = toolbar
        fridayFrom1.inputView = fridayFromPicker1
        
    }
    func showFridayToPicker1(){
        //Formate Date
        saturdayFromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedate1Picker13));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fridayTo1.inputAccessoryView = toolbar
        fridayTo1.inputView = fridayToPicker1
        
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        saturdayFrom.text = formatter.string(from: saturdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func donedatePicker1(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        saturdayTo.text = formatter.string(from: saturdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker2(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        sundayFrom.text = formatter.string(from: sundayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker3(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        sundayTo.text = formatter.string(from: sundayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker4(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        mondayFrom.text = formatter.string(from: mondayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker5(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        mondayTo.text = formatter.string(from: mondayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker6(){
        let formatter = DateFormatter()
        formatter.dateFormat = "H:M:S"
        tuesdayFrom.text = formatter.string(from: tuesdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker7(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        tuesdayTo.text = formatter.string(from: tuesdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker8(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        wednesdayFrom.text = formatter.string(from: wednesdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker9(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        wednesdayTo.text = formatter.string(from: wednesdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker10(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        thursdayFrom.text = formatter.string(from: thursdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker11(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        thursdayTo.text = formatter.string(from: thursdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker12(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fridayFrom.text = formatter.string(from: fridayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker13(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fridayTo.text = formatter.string(from: fridayToPicker.date)
        self.view.endEditing(true)
    }
    //
    @objc func donedate1Picker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        saturdayFrom1.text = formatter.string(from: saturdayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker1(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        saturdayTo1.text = formatter.string(from: saturdayToPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker2(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        sundayFrom1.text = formatter.string(from: sundayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker3(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        sundayTo1.text = formatter.string(from: sundayToPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker4(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        mondayFrom1.text = formatter.string(from: mondayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker5(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        mondayTo1.text = formatter.string(from: mondayToPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker6(){
        let formatter = DateFormatter()
        formatter.dateFormat = "H:M:S"
        tuesdayFrom1.text = formatter.string(from: tuesdayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker7(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        tuesdayTo1.text = formatter.string(from: tuesdayToPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker8(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        wednesdayFrom1.text = formatter.string(from: wednesdayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker9(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        wednesdayTo1.text = formatter.string(from: wednesdayToPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker10(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        thursdayFrom1.text = formatter.string(from: thursdayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker11(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        thursdayTo1.text = formatter.string(from: thursdayToPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker12(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fridayFrom1.text = formatter.string(from: fridayFromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedate1Picker13(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fridayTo1.text = formatter.string(from: fridayToPicker1.date)
        self.view.endEditing(true)
    }
    @IBAction func addSchedule(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.saturdayFrom.text != "" && self.saturdayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.saturdayFrom.text ?? "", toInside: self.saturdayTo.text ?? "", fromOutside: self.saturdayFrom1.text ?? "", toOutside: self.saturdayTo1.text ?? "" , day: 1)
            }
            if self.sundayFrom.text != "" && self.sundayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.sundayFrom.text ?? "", toInside: self.sundayTo.text ?? "", fromOutside: self.sundayFrom1.text ?? "", toOutside: self.sundayTo1.text ?? "", day: 2)
            }
            if self.mondayFrom.text != "" && self.mondayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.mondayFrom.text ?? "", toInside: self.mondayTo.text ?? "", fromOutside: self.mondayFrom1.text ?? "", toOutside: self.mondayTo1.text ?? "", day: 3)
            }
            if self.tuesdayFrom.text != "" && self.tuesdayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.tuesdayFrom.text ?? "", toInside: self.tuesdayTo.text ?? "", fromOutside: self.tuesdayFrom1.text ?? "", toOutside: self.tuesdayTo1.text ?? "", day:4)
            }
            if self.wednesdayFrom.text != "" && self.wednesdayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.wednesdayFrom.text ?? "", toInside: self.wednesdayTo.text ?? "", fromOutside: self.wednesdayFrom1.text ?? "", toOutside: self.wednesdayTo1.text ?? "", day: 5)
            }
            if self.thursdayFrom.text != "" && self.thursdayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.thursdayFrom.text ?? "", toInside: self.thursdayTo.text ?? "", fromOutside: self.thursdayFrom1.text ?? "", toOutside: self.thursdayTo1.text ?? "", day: 6)
            }
            if self.fridayFrom.text != "" && self.fridayTo.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.fridayFrom.text ?? "", toInside: self.fridayTo.text ?? "", fromOutside: self.fridayFrom1.text ?? "", toOutside: self.fridayTo1.text ?? "", day: 7)
            }
            if self.saturdayFrom1.text != "" && self.saturdayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.saturdayFrom.text ?? "", toInside: self.saturdayTo.text ?? "", fromOutside: self.saturdayFrom1.text ?? "", toOutside: self.saturdayTo1.text ?? "" , day: 1)
            }
            if self.sundayFrom1.text != "" && self.sundayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.sundayFrom.text ?? "", toInside: self.sundayTo.text ?? "", fromOutside: self.sundayFrom1.text ?? "", toOutside: self.sundayTo1.text ?? "", day: 2)
            }
            if self.mondayFrom1.text != "" && self.mondayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.mondayFrom.text ?? "", toInside: self.mondayTo.text ?? "", fromOutside: self.mondayFrom1.text ?? "", toOutside: self.mondayTo1.text ?? "", day: 3)
            }
            if self.tuesdayFrom1.text != "" && self.tuesdayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.tuesdayFrom.text ?? "", toInside: self.tuesdayTo.text ?? "", fromOutside: self.tuesdayFrom1.text ?? "", toOutside: self.tuesdayTo1.text ?? "", day:4)
            }
            if self.wednesdayFrom1.text != "" && self.wednesdayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.wednesdayFrom.text ?? "", toInside: self.wednesdayTo.text ?? "", fromOutside: self.wednesdayFrom1.text ?? "", toOutside: self.wednesdayTo1.text ?? "", day: 5)
            }
            if self.thursdayFrom1.text != "" && self.thursdayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.thursdayFrom.text ?? "", toInside: self.thursdayTo.text ?? "", fromOutside: self.thursdayFrom1.text ?? "", toOutside: self.thursdayTo1.text ?? "", day: 6)
            }
            if self.fridayFrom1.text != "" && self.fridayTo1.text != "" {
                self.PostShopSchedule(id: self.id, fromInside: self.fridayFrom.text ?? "", toInside: self.fridayTo.text ?? "", fromOutside: self.fridayFrom1.text ?? "", toOutside: self.fridayTo1.text ?? "", day: 7)
            }
        }
    }
    func PostShopSchedule(id: Int , fromInside:String , toInside:String ,  fromOutside:String , toOutside:String , day:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        let par = ["from_inside": fromInside , "to_inside": toInside , "day_of_week":day , "from_outside": fromOutside , "to_outside": toOutside] as [String : Any]
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddShopSchedule(id: id), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print("successsss")
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
