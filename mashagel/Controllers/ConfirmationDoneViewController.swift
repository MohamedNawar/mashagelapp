//
//  ConfirmationDoneViewController.swift
//  mashagel
//
//  Created by iMac on 10/15/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class ConfirmationDoneViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
