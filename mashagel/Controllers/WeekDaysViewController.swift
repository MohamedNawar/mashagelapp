//
//  WeekDaysViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class WeekDaysViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    var id = Int(){
        didSet{
            //            print(id)
            //         WeekDaysShopsData(id: id)
        }
    }
    var dayId = Int(){
        didSet{
            
        }
    }
    var weekDays = Days(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var currentDay = scheduleData()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDays.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekDaysCell", for: indexPath) as! WeekDaysTableViewCell
        currentDay = (weekDays.data?[indexPath.row])!
        dayId = indexPath.row+1
        if let titleValue = weekDays.data?[indexPath.row].day_of_week {
            let title = cell.viewWithTag(1) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            switch titleValue {
            case 1:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "السبت"
                }else{
                    title.text = NSLocalizedString("Saturday", comment:"السبت")
                }
            case 2:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأحد"
                }else{
                    title.text = NSLocalizedString("Sunday", comment:"الأحد")
                }
            case 3:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأثنين"
                }else{
                    title.text = NSLocalizedString("Monday", comment:"الأثنين")
                }
            case 4:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الثلاثاء"
                }else{
                    title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                }
            case 5:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأربعاء"
                }else{
                    title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                }
            case 6:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الخميس"
                }else{
                    title.text = NSLocalizedString("Thursday", comment:"الخميس")
                }
            case 7:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الجمعة"
                }else{
                    title.text = NSLocalizedString("Friday", comment:"الجمعة")
                }
            default:
                title.text = "\(titleValue)"
            }
        }
        if let titleValue = weekDays.data?[indexPath.row] {
            dayId = weekDays.data?[indexPath.row].id ?? 0
            let title = cell.viewWithTag(3) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if var from = titleValue.from_inside{
                if var to = titleValue.to_inside{
                    var toCount = to.count
                    to.remove(at: String.Index(encodedOffset: toCount))
                    to.remove(at: String.Index(encodedOffset: toCount-1))
                    to.remove(at: String.Index(encodedOffset: toCount-2))
                    var fromCount = from.count
                    from.remove(at: String.Index(encodedOffset: fromCount))
                    from.remove(at: String.Index(encodedOffset: fromCount-1))
                    from.remove(at: String.Index(encodedOffset: fromCount-2))
                    from.characters.removeLast()
                    to.characters.removeLast()
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text =
                             "من الساعة \(from) صباحاإلي الساعة \(to) ليلا داخل الصالون"                                  }else{
                        title.text =
                            NSLocalizedString("from \(from) to \(to) inside Salon", comment:   "من الساعة \(from) صباحاإلي الساعة \(to) ليلا داخل الصالون")                                  }
                        }
            }
        }
        if let titleValue1 = weekDays.data?[indexPath.row] {
            let title = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if var from = titleValue1.from_outside{
                if var to = titleValue1.to_outside{
                    var toCount = to.count
                    to.remove(at: String.Index(encodedOffset: toCount))
                    to.remove(at: String.Index(encodedOffset: toCount-1))
                    to.remove(at: String.Index(encodedOffset: toCount-2))
                    var fromCount = from.count
                    from.remove(at: String.Index(encodedOffset: fromCount))
                    from.remove(at: String.Index(encodedOffset: fromCount-1))
                    from.remove(at: String.Index(encodedOffset: fromCount-2))
                    from.characters.removeLast()
                    to.characters.removeLast()
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون"

                                                      }else{
                            title.text = NSLocalizedString("from \(from) to \(to) outside Salon", comment:   "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون")
                }
                }
            }
        }
        cell.editWeekDayCallBack = {
            self.mangeschedule(Cellid:indexPath.row+1)
            
        }
        return cell
    }
    private func mangeschedule(Cellid:Int){
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "EditWeekDayViewController") as! EditWeekDayViewController
        navVC.day = Cellid
        print(Cellid)
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        print(currentID)
        WeekDaysShopsData(id: currentID)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        print(currentID)
        WeekDaysShopsData(id: currentID)
    }
    
    func WeekDaysShopsData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showSchedule(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekDays = try JSONDecoder().decode(Days.self, from: response.data!)
                        print(self.weekDays)
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func addScheduleDays(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddScheduleViewController") as! AddScheduleViewController
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
    
}

