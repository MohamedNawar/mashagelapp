//
//  RootViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import SideMenu
import GoogleSignIn

class RootViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tblTableView: UITableView!
    
    var imagesArray  = [#imageLiteral(resourceName: "img38"),#imageLiteral(resourceName: "icons8-star-64"),#imageLiteral(resourceName: "icons8-info-50"),#imageLiteral(resourceName: "icons8-contacts-80"),#imageLiteral(resourceName: "icons8-lock-50"),#imageLiteral(resourceName: "icons8-exit-50")]
    var imagesArray1 = [#imageLiteral(resourceName: "icons8-enter-50"),#imageLiteral(resourceName: "icons8-enter-50"),#imageLiteral(resourceName: "img21-1"),#imageLiteral(resourceName: "img19-1"),#imageLiteral(resourceName: "icons8-lock-50")]
    var nameArray1  = [String]()
    var nameArray  = [String]()
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tblTableView.delegate = self
        tblTableView.dataSource = self
        tblTableView.separatorStyle = .none
        userData.Instance.fetchUser()
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
     
        if L102Language.currentAppleLanguage() == "ar" {
//            nameArray1 = ["الحجوزات",
//                          "من نحن",
//                          "تواصل معنا",
//                          "التقييم",
//                          "الشروط و الأحكام",
//                          "تسجيل الدخول"]
//            nameArray = ["الحجوزات",
//                         "من نحن",
//                         "تواصل معنا",
//                         "التقييم",
//                         "الشروط و الأحكام" , "تسجيل الخروج"]
//
            
            nameArray1 = ["تسجيل الدخول",
                          "تسجيل دخول صالون",
                          "من نحن",
                          "تواصل معنا",
                          "الشروط و الأحكام"]
            
            
            nameArray = ["طلباتي",
                         "تقييم",
                         "من نحن",
                         "تواصل معنا",
                         "الشروط و الأحكام" ,
                         "تسجيل الخروج"]

            
        }else{
            
            nameArray1 = [NSLocalizedString("Sign in", comment: "تسجيل الدخول"),
                          NSLocalizedString("Sign in as salon", comment: "تسجيل دخول صالون"),
                          NSLocalizedString("About Us", comment: "من نحن"),
                          NSLocalizedString("Contact Us", comment: "تواصل معنا"),
                          NSLocalizedString("Terms and Conditions", comment: "الشروط و الأحكام")
                          ]
            
            nameArray = [NSLocalizedString("My Orders", comment: "طلباتي"),
                         NSLocalizedString("Rates", comment: "تقييم"),
                         NSLocalizedString("About Us", comment: "من نحن"),
                         NSLocalizedString("Contact Us", comment: "تواصل معنا"),
                         NSLocalizedString("Terms and Conditions", comment: "الشروط و الأحكام"),
                         NSLocalizedString("Sign Out", comment: "تسجيل الخروج")
            ]
//            nameArray1 = [NSLocalizedString("Reservations", comment: "الحجوزات"),
//                          NSLocalizedString("About Us", comment: "من نحن"),
//                          NSLocalizedString("Contact Us", comment: "تواصل معنا"),
//                          NSLocalizedString("Evaluation", comment: "التقييم"),
//                          NSLocalizedString("Terms and Conditions", comment: "الشروط و الأحكام") , NSLocalizedString("Sign In", comment: "تسجيل الدخول")]
//            nameArray = [NSLocalizedString("Reservations", comment: "الحجوزات"),
//                         NSLocalizedString("About Us", comment: "من نحن"),
//                         NSLocalizedString("Contact Us", comment: "تواصل معنا"),
//                         NSLocalizedString("Evaluation", comment: "التقييم"),
//                         NSLocalizedString("Terms and Conditions", comment: "الشروط و الأحكام") , NSLocalizedString("Sign Out", comment: "تسجيل الخروج")]
       
        
        
        
        
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userData.Instance.token == "" || userData.Instance.token == nil {
            return 5
        }else{
            return 6
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblTableView.dequeueReusableCell(withIdentifier: "SlideMenuCell", for: indexPath)
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
        if userData.Instance.token == "" || userData.Instance.token == nil {
            image.image = imagesArray1[indexPath.row]
        }else{
            image.image = imagesArray[indexPath.row]
        }
        
        if userData.Instance.token == "" || userData.Instance.token == nil {
            lbl.text = nameArray1[indexPath.row]
        }else{
            lbl.text = nameArray[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0 :
            // sign in  || my orders
            if userData.Instance.token == "" || userData.Instance.token == nil {
                let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.present(navVC, animated: true)
                
            }else{
                let navVC = storyboard?.instantiateViewController(withIdentifier: "ShopReservation11ViewController") as! ShopReservation11ViewController
                self.navigationController?.pushViewController(navVC, animated: true)
            }
            
        case 1:
            // sign in as salon ||  rates
            if userData.Instance.token == "" || userData.Instance.token == nil {
                let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.present(navVC, animated: true)
            }else{
                print("rates")
            }
        case 2 :
            // about us
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case 3:
            // contact us
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case 4:
            // terms & conditions
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case 5:
            // log out
           userData.Instance.remove()
           userData.Instance.removeShopID()
           userData.Instance.fetchUser()
//           NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
//           self.dismiss(animated: true, completion: nil)
           
            if GIDSignIn.sharedInstance().hasAuthInKeychain() {
                GIDSignIn.sharedInstance().signOut()
            }
            makeErrorAlert(title: "Log out", SubTitle: "Are you sure you want to logout", Image: #imageLiteral(resourceName: "img34"))
           
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
         //   let navVc = UINavigationController(rootViewController: vc)
            self.present(vc, animated: true, completion: nil)
            
        default:
            break
        }
    }
}
