//
//  MyReservationViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Parchment

class MyReservationViewController:UIViewController {
    
    @IBOutlet weak var MainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let firstViewController = storyboard?.instantiateViewController(withIdentifier:"PreviousReservationViewController") as! PreviousReservationViewController
        
        let secondViewController = storyboard?.instantiateViewController(withIdentifier:"CurrentReservationViewController") as! CurrentReservationViewController
        
        firstViewController.title = NSLocalizedString("Previous", comment: "السابقة")
        secondViewController.title = NSLocalizedString("Reservationscurrent", comment: "الحالية")
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController,
            ])
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        pagingViewController.indicatorColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1);    pagingViewController.selectedBackgroundColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        pagingViewController.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        pagingViewController.font = UIFont(name: "Cairo", size: 15) ?? UIFont()
        pagingViewController.selectedFont = UIFont(name: "Cairo", size: 15) ?? UIFont()
        pagingViewController.backgroundColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        pagingViewController.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: view.frame.width/2 , height: pagingViewController.menuItemSize.height + 10 )
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        
        pagingViewController.menuInteraction = .none
        
        pagingViewController.selectedScrollPosition = .preferCentered
        addChildViewController(pagingViewController)
        MainView.addSubview(pagingViewController.view)
        
        
        pagingViewController.didMove(toParentViewController: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        
        NSLayoutConstraint.activate([
            pagingViewController.view.leadingAnchor.constraint(equalTo: MainView.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: MainView.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: MainView.bottomAnchor),
            pagingViewController.view.topAnchor.constraint(equalTo: MainView.topAnchor )
            ])
    }
    
    
}


