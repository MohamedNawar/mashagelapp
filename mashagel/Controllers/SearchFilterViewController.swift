//
//  SearchFilterViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class SearchFilterViewController:  UIViewController , UITextFieldDelegate , UIPickerViewDelegate , UIPickerViewDataSource , UITextViewDelegate {
    
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var sectionTextField: UITextField!
    @IBOutlet weak var searchTextField: DesignableTextField!
    
    let picker = UIPickerView()
    let words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    let picker1 = UIPickerView()
    let words1 = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected1 = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        
        picker1.delegate = self
        picker.delegate = self
        districtTextField.delegate = self
        cityTextField.delegate = self
        sectionTextField.delegate = self
        searchTextField.delegate = self; districtTextField.addPadding(UITextField.PaddingSide.left(20))
        cityTextField.addPadding(UITextField.PaddingSide.left(20))
        sectionTextField.addPadding(UITextField.PaddingSide.left(20))
        cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        districtTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        
        showCityPicker()
        showDistrictPicker()
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTextField.inputView = picker
        cityTextField.inputAccessoryView = toolbar
    }
    
    @objc func cancel(){
        cityTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        if cityTextField.text == "" {
            cityTextField.text = words[0] ?? ""
        }
        self.view.endEditing(true)
    }
    func showDistrictPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        districtTextField.inputView = picker1
        districtTextField.inputAccessoryView = toolbar
    }
    
    @objc func cancel1(){
        districtTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker1(){
        if districtTextField.text == "" {
            districtTextField.text = words1[0] ?? ""
        }
        self.view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker {
            return words.count
        }
        if pickerView == picker1 {
            return words1.count
        }
        
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker {
            return "\(words[row])"
            
        }
        if pickerView == picker1 {
            return "\(words1[row])"
        }
        
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker {
            selected = words[row]
            cityTextField.text = selected
        }
        if pickerView == picker1 {
            selected1 = words1[row]
            districtTextField.text = selected1
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
