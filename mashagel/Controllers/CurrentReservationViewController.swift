//
//  CurrentReservationViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class CurrentReservationViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentReservationCell1", for: indexPath)
            return cell
        }
        if indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentReservationCell3", for: indexPath)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentReservationCell2", for: indexPath)
        return cell
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()

    }
}

