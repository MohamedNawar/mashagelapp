//
//  HeaderViewController.swift
//  mashagel
//
//  Created by iMac on 10/31/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Auk
import moa
import PKHUD
import Alamofire
import FCAlertView
class HeaderViewController: UIViewController {
    var shopData = ShopData()
//    var id = Int(){
//        didSet{
//
//            print(id)
//
//        }
//    }
    var images = [String](){
        didSet{
            
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
    override func viewDidLoad() {
        super.viewDidLoad()

      scrollView.auk.settings.contentMode = .scaleToFill
        setScrollView()
    }
    private func setScrollView(){
       // self.images.removeAll()
        
        if let  imageIndex = self.shopData.images   {
            for img in imageIndex{
                
                self.images.append(img.large ?? "")
            }
        }

            for image in images {
                scrollView.auk.show(url:image )
                
                
            }
    }

}
