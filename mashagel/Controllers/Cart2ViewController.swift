////
////  Cart2ViewController.swift
////  mashagel
////
////  Created by MACBOOK on 10/1/18.
////  Copyright © 2018 MohamedHassanNawar. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import PKHUD
//import FCAlertView
//import Font_Awesome_Swift
//class Cart2ViewController:  UIViewController , UITextFieldDelegate ,  UITableViewDelegate , UITableViewDataSource  {
//    @IBAction func dismiss(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }
//    var reservation = Reservation(){
//        didSet{
//            if let dateValue = reservation.data?.date?.date {
//                var stringValue = dateValue
//                stringValue = stringValue.components(separatedBy: " ")[0]
//                dateTextField.text = stringValue
//            }else{
//                if let date = userData.Instance.selectedDate {
//                     dateTextField.text = date
//                }
//            }
//            if let priceValue = reservation.data?.total_price{
//                totalPriceLabl.text = "$\(priceValue)"
//            }
//            
//            self.tableView.reloadData()
//        }
//    }
//    var currentService = ServicesData()
//    var currentPackage = Package_item()
//    var headerHeight = Int()
//    var sectionsCount = Int()
//    let datePicker = UIDatePicker()
//    var reserVationId = Int()
//    @IBOutlet weak var hiddentextField: UITextField!
//    @IBOutlet weak var dateTextField: DesignableTextField!
//    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var totalPriceLabl: UILabel!
//    @IBOutlet weak var image: UIImageView!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//         self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
//        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
//        self.title = NSLocalizedString("Reservations", comment: "الحجوزات")
//         navigationItem.title = NSLocalizedString("Reservations", comment: "الحجوزات")
//        userData.Instance.fetchUser()
//        tableView.allowsSelection = false
//        datePicker.backgroundColor = .white
//        reserVationId = userData.Instance.identifier ?? -1
//        tableView.separatorStyle = .none
//        tableView.allowsSelection = false
//        hiddentextField.delegate = self
//        userData.Instance.fetchUser()
//        PreparingReservationShopData(id: userData.Instance.identifier ?? -1)
//        hiddentextField.addPadding(UITextField.PaddingSide.left(20))
//        hiddentextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
//        showDatePicker()
//        tabBarItem.imageInsets.top = -6 
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        if L102Language.currentAppleLanguage() == "ar"{
//            self.title = "الحجوزات"
//            navigationItem.title =  "الحجوزات"
//        }else{
//            self.title = NSLocalizedString("Reservations", comment: "الحجوزات")
//            navigationItem.title = NSLocalizedString("Reservations", comment: "الحجوزات")
//        }
//        userData.Instance.fetchUser()
//        if let dateValue = reservation.data?.date?.date {
//            var stringValue = dateValue
//            stringValue = stringValue.components(separatedBy: " ")[0]
//            dateTextField.text = stringValue
//        }else{
//            if let date = userData.Instance.selectedDate {
//                dateTextField.text = date
//                PreparingSendDateForCurrentReservation(id:reserVationId)
//            }
//        }
//        PreparingReservationShopData(id: userData.Instance.identifier ?? -1)
//        if L102Language.currentAppleLanguage() == "ar" {
//            image.image = UIImage(named: "img02")
//        }
//    }
//    func showDatePicker(){
//        //Formate Date
//        datePicker.datePickerMode = .date
//        //ToolBar
//        let toolbar = UIToolbar();
//        toolbar.sizeToFit()
//        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
//        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
//        
//        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
//        
//        dateTextField.inputAccessoryView = toolbar
//        dateTextField.inputView = datePicker
//        
//    }
//    @objc func donedatePicker(){
//        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        dateTextField.text = formatter.string(from: datePicker.date)
//         PreparingSendDateForCurrentReservation(id:reserVationId)
//        self.view.endEditing(true)
//    }
//    
//    @objc func cancelDatePicker(){
//        self.view.endEditing(true)
//    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
//        textField.borderWidth = 1
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField.text == "" {
//            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
//        }
//    }
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        let service = reservation.data?.service_items?.items?.count ?? 0
//        if service > 0 {
//            return ( reservation.data?.service_items?.package_items?.count ?? 0)+1
//        }else{
//            return reservation.data?.service_items?.package_items?.count ?? 0
//        }
//        
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let service = reservation.data?.service_items?.items?.count ?? 0
//        if service > 0 {
//            if section == reservation.data?.service_items?.package_items?.count {
//                return service
//            }
//            return reservation.data?.service_items?.package_items?[section].items?.count ?? 0
//        }else{
//            return reservation.data?.service_items?.package_items?[section].items?.count ?? 0
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cart2Cell1", for: indexPath) as!  Cart2ContentCell
//        let package = reservation.data?.service_items?.package_items?.count ?? 0
//        if  package > 0 {
//            let service = reservation.data?.service_items?.items?.count ?? 0
//            if service > 0 {
//                if indexPath.section == (reservation.data?.service_items?.package_items?.count ?? 0) {
//                    cell.deleteBtn.isEnabled = true
//                    cell.deleteBtn.isHidden = false
//                    cell.updateViews(service:reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData())
//                    currentService = reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData()
//                    
//                    cell.DeleteServiceCallBack = {
//                        () -> () in
//                        self.deleteServiceInReservationShopData(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)
//                        self.reservation.data?.service_items?.items?.remove(at: indexPath.row)
//                        if self.reservation.data?.service_items?.items?.count == 0 {
//                            userData.Instance.removeIdentifier()
//                        }
//                        self.tableView.reloadData()
//                        
//                    }
//                    cell.addEmployeeServiceCallBack = {
//                        (employeeID:Int) -> () in
//                        if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVationId)
//                        }
//                        //                       if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
//                        //                        }
//                        self.tableView.reloadData()
//                    }
//                    cell.addTimerServiceCallBack = {
//                        (selectedTime:String) -> () in
//                        if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeTimer(itemId:serviceForItem , time : selectedTime, identifierId: self.reserVationId)
//                        }
//                        //                        if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
//                        //                        }
//                        self.tableView.reloadData()
//                    }
//                }else{
//                cell.deleteBtn.isEnabled = false
//                cell.deleteBtn.isHidden = true
//                cell.updateViews(service:reservation.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData())
//                currentService = reservation.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData()
//                cell.DeleteServiceCallBack = {
//                    () -> () in
//                    self.deleteServiceInReservationShopData(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)
//                    self.reservation.data?.service_items?.package_items?[indexPath.section].items?.remove(at: indexPath.row)
//                    if self.reservation.data?.service_items?.items?.count == 0 {
//                        userData.Instance.removeIdentifier()
//                    }
//                    self.tableView.reloadData()
//                    
//                }
//                cell.addEmployeeServiceCallBack = {
//                    (employeeID:Int) -> () in
//                    if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVationId)
//                    }
//                    //                       if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
//                    //                        }
//                    self.tableView.reloadData()
//                }
//                cell.addTimerServiceCallBack = {
//                    (selectedTime:String) -> () in
//                    if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeTimer(itemId:serviceForItem , time : selectedTime, identifierId: self.reserVationId)
//                    }
//                    //                        if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
//                    //                        }
//                    self.tableView.reloadData()
//                }
//                }
//            }else{
//                cell.deleteBtn.isEnabled = false
//                cell.deleteBtn.isHidden = true; cell.updateViews(service:reservation.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData())
//                currentService = reservation.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData()
//                cell.addEmployeeServiceCallBack = {
//                    (employeeID:Int) -> () in
//                    
//                    if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
//                    }
//                    self.tableView.reloadData()
//                }
//                cell.addTimerServiceCallBack = {
//                    (selectedTime:String) -> () in
//                    if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
//                    }
//                    self.tableView.reloadData()
//                }
//                cell.DeleteServiceCallBack = {
//                    () -> () in
//                    self.deleteServiceInReservationShopData(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)
//                    self.reservation.data?.service_items?.package_items?[indexPath.section].items?.remove(at: indexPath.row)
//                    if self.reservation.data?.service_items?.items?.count == 0 {
//                        userData.Instance.removeIdentifier()
//                    }
//                    self.tableView.reloadData()
//                }
//            }
//        }else{
//            cell.deleteBtn.isEnabled = true
//            cell.deleteBtn.isHidden = false;
//            let service = reservation.data?.service_items?.items?.count ?? 0
//            if service > 0 {
//                if indexPath.section == (reservation.data?.service_items?.package_items?.count ?? 0) {
//                    cell.updateViews(service:reservation.data?.service_items?.items?[indexPath.section] ?? ServicesData())
//                    currentService = reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData()
//                    cell.addEmployeeServiceCallBack = {
//                        (employeeID:Int) -> () in
//                        if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVationId)
//                        }
//                        //                       if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
//                        //                        }
//                        self.tableView.reloadData()
//                    }
//                    cell.addTimerServiceCallBack = {
//                        (selectedTime:String) -> () in
//                        if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeTimer(itemId:serviceForItem , time : selectedTime, identifierId: self.reserVationId)
//                        }
//                        //                        if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
//                        //                        }
//                        self.tableView.reloadData()
//                    }
//                    cell.DeleteServiceCallBack = {
//                        () -> () in
//                        self.deleteServiceInReservationShopData(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)
//                        self.reservation.data?.service_items?.items?.remove(at: indexPath.row)
//                        if self.reservation.data?.service_items?.items?.count == 0 {
//                            userData.Instance.removeIdentifier()
//                        }
//                        self.tableView.reloadData()
//                        
//                    }
//                }
//            }
//        }
//        return cell
//        
//    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        
//        let headerCell = tableView.dequeueReusableCell(withIdentifier: "Cart2Cell11") as! Cart2HeaderCell
//        headerCell.updateViews(Package: reservation.data?.service_items?.package_items?[section] ?? Package_item())
//        headerCell.DeleteServiceCallBack = {
//            () -> () in
//            self.deletePackageInReservationShopData(id:self.reservation.data?.service_items?.package_items?[section].id ?? -1 , userId : self.reserVationId)
//            self.reservation.data?.service_items?.package_items?.remove(at: section)
//            if self.reservation.data?.service_items?.items?.count == 0 {
//                userData.Instance.removeIdentifier()
//            }
//            self.tableView.reloadData()
//            
//            
//        }
//        
//        return headerCell
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        let package = reservation.data?.service_items?.package_items?.count ?? 0
//        if  package > 0 {
//            let service = reservation.data?.service_items?.items?.count ?? 0
//            if service > 0 {
//                if section == (reservation.data?.service_items?.package_items?.count ?? 0) {
//                    return CGFloat(0)
//                }
//                return CGFloat(50)
//            }
//        }else{
//            return CGFloat(0)
//        }
//        return CGFloat(50)
//    }
//    func SetEmployeeId(itemId:Int , employeeId:Int ,identifierId:Int ){
//        var header = APIs.Instance.getHeader()
//        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
//        print(APIs.Instance.setEmployeeId(id:itemId, identifier: identifierId))
//        print(header)
//        HUD.show(.progress)
//        let par = ["employee_id": employeeId] as [String : Any];
//        print(par)
//        Alamofire.request(APIs.Instance.setEmployeeId(id:itemId, identifier: identifierId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
//            .responseJSON { (response:DataResponse<Any>) in
//                
//                switch(response.result) {
//                case .success(let value):
//                    print(value)
//                    HUD.hide()
//                    print(value)
//                    let temp = response.response?.statusCode ?? 400
//                    if temp >= 300 {
//                        
//                        do {
//                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
//                            print(err.errors)
//                        }catch{
//                            print("errorrrrelse")
//                        }
//                    }else{
//                        do {
//                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                            
//                        }catch{
//                            print(error)
//                            HUD.flash(.label("Error Try Again"), delay: 1.0)
//                        }
//                    }
//                case .failure(_):
//                    HUD.hide()
//                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                    HUD.flash(.label(lockString), delay: 1.0)
//                    break
//                }
//        }
//    }
//    func SetEmployeeTimer(itemId:Int , time:String,identifierId:Int ){
//        var header = APIs.Instance.getHeader()
//        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
//        print(header)
//        HUD.show(.progress)
//        let par = ["due_time": time] as [String : Any];
//        print(par)
//        Alamofire.request(APIs.Instance.setDuetime(id:itemId, identifier: identifierId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
//            .responseJSON { (response:DataResponse<Any>) in
//                
//                switch(response.result) {
//                case .success(let value):
//                    print(value)
//                    HUD.hide()
//                    print(value)
//                    let temp = response.response?.statusCode ?? 400
//                    if temp >= 300 {
//                        
//                        do {
//                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
//                            print(err.errors)
//                        }catch{
//                            print("errorrrrelse")
//                        }
//                    }else{
//                        do {
//                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                            
//                        }catch{
//                            print(error)
//                            HUD.flash(.label("Error Try Again"), delay: 1.0)
//                        }
//                    }
//                case .failure(_):
//                    HUD.hide()
//                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                    HUD.flash(.label(lockString), delay: 1.0)
//                    break
//                }
//        }
//    }
//    func deletePackageInReservationShopData(id:Int , userId : Int){
//        let header = APIs.Instance.getHeader()
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.Instance.RemovePackage(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//                    
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                        
//                    }catch{
//                        print(error)
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                HUD.flash(.label(lockString), delay: 1.0)
//                break
//            }
//        }
//    }
//    
//    func deleteServiceInReservationShopData(id:Int , userId : Int){
//        let header = APIs.Instance.getHeader()
//        let url = (APIs.Instance.RemoveService(id: id, identifier: userId))
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                print(url)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//                    
//                    
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                        
//                    }catch{
//                        print(error)
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                HUD.flash(.label(lockString), delay: 1.0)
//                break
//            }
//        }
//    }
//    func PreparingReservationShopData(id:Int){
//        let header = APIs.Instance.getHeader()
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.Instance.CurrentReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//                    
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                        
//                    }catch{
//                        print(error)
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                HUD.flash(.label(lockString), delay: 1.0)
//                break
//            }
//        }
//    }
//    func PreparingSendDateForCurrentReservation(id:Int){
//        var header = APIs.Instance.getHeader()
//        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
//        
//        print(header)
//        HUD.show(.progress, onView: self.view)
//        let par = ["date": dateTextField.text!] as [String : Any];
//        print(par)
//        Alamofire.request(APIs.Instance.SendDateForCurrentReservation(id:reserVationId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
//            .responseJSON { (response:DataResponse<Any>) in
//                
//                switch(response.result) {
//                case .success(let value):
//                    print(value)
//                    HUD.hide()
//                    print(value)
//                    let temp = response.response?.statusCode ?? 400
//                    if temp >= 300 {
//                        
//                        do {
//                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                            print(err.errors)
//                        }catch{
//                            print("errorrrrelse")
//                        }
//                    }else{
//                        do {
//                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                            
//                        }catch{
//                            print(error)
//                            HUD.flash(.label("Error Try Again"), delay: 1.0)
//                        }
//                    }
//                case .failure(_):
//                    HUD.hide()
//                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                    HUD.flash(.label(lockString), delay: 1.0)
//                    break
//                }
//        }
//    }
//    func confirmReservationShopData(userId : Int){
//        let header = APIs.Instance.getHeader()
//        HUD.show(.progress, onView: self.view)
//        print(APIs.Instance.ConfirmReservation(identifier: userId))
//        Alamofire.request(APIs.Instance.ConfirmReservation(identifier: userId) , method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//                    
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
//                        self.performSegue(withIdentifier: "popOver", sender: self)
//                        userData.Instance.removeIdentifier()
//                        self.reservation.data?.service_items?.items?.removeAll()
//                        self.reservation.data?.packages?.removeAll()
//                        self.tableView.reloadData()
//                    }catch{
//                        print(error)
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
//                HUD.flash(.label(lockString), delay: 1.0)
//                break
//            }
//        }
//    }
//    
//    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
//        let alert = FCAlertView()
//        alert.avoidCustomImageTint = true
//        let updatedFrame = alert.bounds
//        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
//        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
//    }
//    
//    @IBAction func confirmReservation(_ sender: Any) {
//        if userData.Instance.token == "" || userData.Instance.token == nil {
//            self.performSegue(withIdentifier: "popOver11", sender: self)
//            
//        }else{
//            let userIdentifier = userData.Instance.identifier
//            print(userIdentifier)
//            confirmReservationShopData(userId: userIdentifier ?? -1)        }
//    }
//    @IBAction func changeLanguage(_ sender: Any) {
//        if L102Language.currentAppleLanguage() == "ar"{
//            L102Language.setAppleLAnguageTo(lang: "en")
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//        } else {
//            L102Language.setAppleLAnguageTo(lang: "ar")
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            
//        };
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarControllerViewController")
//        let mainwindow = (UIApplication.shared.delegate?.window!)!
//        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
//        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
//        }) { (finished) -> Void in
//        }
//    }
//    
//}
//
