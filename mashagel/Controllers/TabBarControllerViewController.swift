//
//  TabBarControllerViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/6/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class TabBarControllerViewController: UITabBarController {
    var id = Int()
    @objc func methodOfReceivedNotification(notification: Notification) {
        userData.Instance.fetchUser()
        if userData.Instance.token == "" || userData.Instance.token == nil {
            setupTabBar()
            self.selectedIndex = 0
        }else{
            if userData.Instance.data?.type == 2 {
                setupTabBarwhenShopOwnerLogin()
                self.selectedIndex = 0
            }else{
                setupTabBarwhenUserLoginAndReserveOutSide()
                self.selectedIndex = 0
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        if userData.Instance.token == "" || userData.Instance.token == nil {
            setupTabBar()
            self.selectedIndex = 0
        }else{
            if userData.Instance.data?.type == 2 {
                setupTabBarwhenShopOwnerLogin()
                self.selectedIndex = 0
            }else{
                
                setupTabBarwhenUserLoginAndReserveOutSide()
                self.selectedIndex = 0
                
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    func setupTabBarwhenShopOwnerLogin(){
        let serachVC = storyboard?.instantiateViewController(withIdentifier: "AllBeautyCenterForAdminViewController")
        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
        firstViewController.tabBarItem.image = UIImage(named: "img10")
        let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController")
        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
        secondViewController.tabBarItem.image = UIImage(named: "img08")
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController")
        let thirdViewController = UINavigationController(rootViewController: profileVC ?? UIViewController())
        thirdViewController.tabBarItem.image = UIImage(named: "img09")
        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        let fourthViewController = UINavigationController(rootViewController: notificationVC ?? UIViewController())
        fourthViewController.tabBarItem.image = UIImage(named: "img07")
        viewControllers = [ firstViewController,secondViewController ,thirdViewController , fourthViewController ]
        self.setViewControllers(viewControllers, animated: true)
    }
    func setupTabBar(){
        let serachVC = storyboard?.instantiateViewController(withIdentifier: "SearchViewController")
        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
        firstViewController.tabBarItem.image = UIImage(named: "img10")
        let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController")
        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
        secondViewController.tabBarItem.image = UIImage(named: "img08")
        viewControllers = [firstViewController,secondViewController]
        self.setViewControllers(viewControllers, animated: true)
    }
    func employeeSetupTabBar(){
        let serachVC = storyboard?.instantiateViewController(withIdentifier: "ShopReservation11ViewController")
        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
        firstViewController.tabBarItem.image = UIImage(named: "img10")
        let cartVC = storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController")
        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
        secondViewController.tabBarItem.image = UIImage(named: "img09")
        viewControllers = [firstViewController ,  secondViewController]
        self.setViewControllers(viewControllers, animated: true)
    }
    //    func setupTabBarwhenUserLogin(){
    //        let serachVC = storyboard?.instantiateViewController(withIdentifier: "SearchViewController")
    //        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
    //        firstViewController.tabBarItem.image = UIImage(named: "img10")
    //        let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart2ViewController")
    //        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
    //        secondViewController.tabBarItem.image = UIImage(named: "img08")
    //        let profileVC = storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController")
    //        let thirdViewController = UINavigationController(rootViewController: profileVC ?? UIViewController())
    //        thirdViewController.tabBarItem.image = UIImage(named: "img09")
    //        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
    //        let fourthViewController = UINavigationController(rootViewController: notificationVC ?? UIViewController())
    //        fourthViewController.tabBarItem.image = UIImage(named: "img07")
    //        viewControllers = [secondViewController , thirdViewController , fourthViewController , firstViewController]
    //        self.setViewControllers(viewControllers, animated: true)
    //    }
    func setupTabBarwhenUserLoginAndReserveOutSide(){
        let serachVC = storyboard?.instantiateViewController(withIdentifier: "SearchViewController")
        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
        firstViewController.tabBarItem.image = UIImage(named: "img10")
        let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController")
        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
        secondViewController.tabBarItem.image = UIImage(named: "img08")
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController")
        let thirdViewController = UINavigationController(rootViewController: profileVC ?? UIViewController())
        thirdViewController.tabBarItem.image = UIImage(named: "img09")
        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        let fourthViewController = UINavigationController(rootViewController: notificationVC ?? UIViewController())
        fourthViewController.tabBarItem.image = UIImage(named: "img07")
        viewControllers = [ firstViewController,secondViewController ,thirdViewController , fourthViewController ]
        self.setViewControllers(viewControllers, animated: true)
    }
}
