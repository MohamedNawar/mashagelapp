//
//  ShowAllEmployeeForShopViewController.swift
//  mashagel
//
//  Created by iMac on 10/14/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class ShowAllEmployeeForShopViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    var id = Int(){
        didSet{
            //            print(id)
            //            OfficialHolidaysShopsData(id: id)
        }
    }
    var selectedEmployeeId = Int()
    var employeeId = Int()
    var employees = employeesData(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var currentDay = scheduleData()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees.data?.count ?? 0
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEmployeeInfo" {
            let destination = segue.destination as! TimeOfWorksEmployeeTabeViewController
            destination.selectedEmployeeId = selectedEmployeeId
            userData.Instance.removeCurrentEmployeeId()
            userData.Instance.currentEmployeeId =  selectedEmployeeId
            UserDefaults.standard.set(selectedEmployeeId, forKey: "currentEmployeeId")
        }
    }
    @IBAction func editEmployee(_ sender: Any) {
        performSegue(withIdentifier: "showEmployeeInfo", sender: self)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowAllEmployeeForShopCell", for: indexPath)
        selectedEmployeeId = employees.data?[indexPath.row].id ?? 0
        employeeId = employees.data?[indexPath.row].id ?? 0
        if let titleValue = employees.data?[indexPath.row].name{
            let title = cell.viewWithTag(1) as! UILabel
            title.text = titleValue
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
        }
        if let titleValue1 = employees.data?[indexPath.row] {
            let title = cell.viewWithTag(2) as! UILabel
            title.text = titleValue1.mobile
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
        }
        if let titleValue = employees.data?[indexPath.row] {
            let title = cell.viewWithTag(3) as! UILabel
            title.text = titleValue.email
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
        }
        
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        ShopsEmployeeData(id: currentID)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        id = userData.Instance.userSelectedShopId as! Int
        ShopsEmployeeData(id: id)
        if L102Language.currentAppleLanguage() == "ar" {
            self.title = "جميع الموظفيين"
        }else{
            self.title = NSLocalizedString("All Employees", comment:"جميع الموظفيين")
        }
    }
    
    @IBAction func addExceptionDay(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddEmployeeViewController") as! AddEmployeeViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    func ShopsEmployeeData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.listEmployee(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.employees = try JSONDecoder().decode(employeesData.self, from: response.data!)
                        print(self.employees)
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
