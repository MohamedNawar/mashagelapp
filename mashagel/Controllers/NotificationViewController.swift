//
//  NotificationViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath)
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        userData.Instance.fetchUser()
        tabBarItem.imageInsets.top = -6
        if L102Language.currentAppleLanguage() == "ar" {
            self.title = "الإشعارات"
            navigationItem.title = "الإشعارات"
        }else{
            self.title = NSLocalizedString("Notification", comment:"الإشعارات")
            navigationItem.title = NSLocalizedString("Notification", comment:"الإشعارات")
        }
    }
}

