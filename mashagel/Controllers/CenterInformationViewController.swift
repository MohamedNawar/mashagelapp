//
//  CenterInformationViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift

class CenterInformationViewController: UIViewController , UITextFieldDelegate , UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 5
    }
    
        
    let picker = UIPickerView()
    let words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""

        @IBOutlet weak var phoneTextField: UITextField!
        @IBOutlet weak var cityTextField: UITextField!
      @IBOutlet weak var addressTextField: DesignableTextField!
        override func viewDidLoad() {
            super.viewDidLoad()
            picker.delegate = self
            picker.dataSource = self
            picker.backgroundColor = .white
            picker.showsSelectionIndicator = true
            phoneTextField.delegate = self
            cityTextField.delegate = self
            addressTextField.delegate = self
            userData.Instance.fetchUser()
            print("eeeeeee")
            print(userData.Instance.userSelectedShopId)
            let currentID = userData.Instance.userSelectedShopId as! Int
 phoneTextField.addPadding(UITextField.PaddingSide.left(20))
            cityTextField.addPadding(UITextField.PaddingSide.left(20))
//            addressTextField.addPadding(UITextField.PaddingSide.left(20))
            cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            showCityPicker()
        }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTextField.inputView = picker
        cityTextField.inputAccessoryView = toolbar
    }
    
    @objc func cancel(){
        cityTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        if cityTextField.text == "" {
            cityTextField.text = words[0] ?? ""
        }
        self.view.endEditing(true)
    }
        func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            if textField.text == "" {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            }
        }
        @IBAction func EditBtn(_ sender: Any) {
            
        }

}

