//
//  OffersInMainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//
import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class OffersInMainViewController:UIViewController , UITableViewDelegate , UITableViewDataSource   {
    var currentServiceID = Int()
    var serviceReservation = Reservation()
    var shopData = Shop(){
        didSet{
            categoryIds =  returnCategoryIDs(shop: shopData)
            categoryNames = returnCategoryNames(shop: shopData)
        }
    }
    var shopID = Int()
    var servicesArray = [ServicesData]()
    //    var id = Int(){
    ////        didSet{
    ////            print(id)
    ////            if id > 0 {
    ////           loadShopData(id: id)
    ////           self.tableView.reloadData()
    ////            }
    ////        }
    //    }
    var categoryNames = Set<String>(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var servicesForCategory = [Int:Int](){
        didSet{
        }
    }
    var categoryIds = Set<Int>(){
        didSet{
            // servicesForCategory = returnServicesCount(shop: shopData)\
            
        }
    }
    var currentID  = Int()
    var serType = "inside"
    var contentCount = 0
    var headerHeigh = 0
    var FooterHeigh = 0
    var reservationIdentifier = Int()
    @IBOutlet weak var serviceType: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        currentID = userData.Instance.userSelectedShopId as! Int
        loadShopData(id: currentID)
        NSLocalizedString("Services", comment:"الخدمات")
         shopID = userData.Instance.userSelectedShopId as! Int
        if serType == "outside" {
                 reservationIdentifier = userData.Instance.identifierOutside ?? -1
        }else{
            reservationIdentifier = userData.Instance.identifierInside ?? -1
        }
        if reservationIdentifier == nil || reservationIdentifier == -1{
            PreparingReservationShopData(id:shopID, type: true)
        }else{
            PreparingReservationShopData(id:shopID, type: false)
        }
    }
    
    func returnCategoryIDs(shop:Shop)->Set<Int>{
        var category = Set<Int>()
        category.removeAll()
        if let services = shopData.data?.services {
            for service in services {
                var categoryId = service.category?.id
                category.insert(categoryId!)
                print(category)
            }
        }
        return category
    }
    func returnCategoryNames(shop:Shop)->Set<String>{
        var category = Set<String>()
        category.removeAll()
        if let services = shopData.data?.services {
            for service in services {
                var categoryName = service.category?.name
                category.insert(categoryName!)
                print(category)
            }
        }
        return category
    }
    //    func returnServicesCount(shop:Shop)->[Int: Any]{
    //        var servicesForCategory = [Int: Any]()
    //        servicesForCategory.removeAll()
    //        var  services =  shopData.data?.services ?? [ServicesData]()
    //        for categoryId in categoryIds {
    //        for service in services {
    //                if service.category?.id == categoryId {
    //                    var serviceIDs = [Int]()
    //                    serviceIDs.append(service.id!)
    //
    //                   servicesForCategory.updateValue(categoryId, forKey:serviceIDs)
    //                }
    //            }
    //        }
    //        return servicesForCategory
    //    }
    //    func returnCategoryNames(shop:Shop)->Set<String>{
    //        var category = Set<String>()
    //        category.removeAll()
    //        if let services = shopData.data?.services {
    //            for service in services {
    //                var categoryName = service.category?.name
    //                category.insert(categoryName!)
    //                print(category)
    //            }
    //        }
    //        return category
    //    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return categoryIds.count ?? 0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
        let shopID = userData.Instance.userSelectedShopId as! Int
        
            reservationIdentifier = userData.Instance.identifierInside ?? -1
            if reservationIdentifier == nil || reservationIdentifier == -1{
                PreparingReservationShopData(id:shopID, type: true)
            }else{
                PreparingReservationShopData(id:shopID, type: false)
        }
        
    }
    func getServiceCount(section:Int)->Int{
        let categoryIdsArray = Array(categoryIds)
        let categoryId = categoryIdsArray[section]
        let services = shopData.data?.services
        services?.filter({return $0.category_id == categoryId})
        if serviceType.selectedSegmentIndex == 1  {
            serType = "outside"
        }else {
            serType = "inside"
        }
        servicesArray.removeAll()
        for ser in services ?? [ServicesData]() {
            if ser.type?.SString == serType  {
                servicesArray.append(ser)
            }
        }
        services?.filter({return $0.type?.SString as! String == serType})
        print(servicesArray.count)
        return (servicesArray.count)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getServiceCount(section:section)+1 ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderOffersCell", for: indexPath) as! servicesHeaderTableViewCell
            let categoryIdsArray = Array(categoryNames)
            if categoryIdsArray.count > 0 {
                let nameValue = categoryIdsArray[indexPath.section]
                let name = cell.viewWithTag(2) as! UILabel
                print(nameValue)
                name.text = "\(nameValue)"
            }
            cell.openAndCloseServiceCallBack = {
                () -> () in
                
                self.tableView.reloadData()
                
            }
            return cell
        }
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "OffersCell", for: indexPath) as! ServicesInCategoryTableViewCell
        let categoryIdsArray = Array(categoryIds)
        let categoryId = categoryIdsArray[indexPath.section]
        let services = shopData.data?.services
        services?.filter({return $0.category_id == categoryId})
        if serviceType.selectedSegmentIndex == 1 {
            serType = "outside"
        }else {
            serType = "inside"
        }
        services?.filter({return $0.type?.SString == serType})
        currentServiceID = servicesArray[indexPath.row-1].id ?? 0
        if let nameValue = servicesArray[indexPath.row-1].name {
            let name = cell2.viewWithTag(1) as! UILabel
            print(nameValue)
            name.text = "\(nameValue)"
            
        }
        if let priceValue = servicesArray[indexPath.row-1].price {
            let price = cell2.viewWithTag(2) as! UILabel
            price.text = "\(priceValue)"
        }
        if let timeValue = servicesArray[indexPath.row-1].amount_of_time?.IInt {
            let time = cell2.viewWithTag(13) as! UILabel
            time.text = "\(timeValue)Minute"
            if L102Language.currentAppleLanguage() == "ar" {
                time.textAlignment = .right
                time.text = " \(timeValue )دقيقة " 
            }
        }
        cell2.ShowServiceImages = {
            () -> () in
            let selectedService =  self.servicesArray[indexPath.row-1]
            self.showCurrentServiceImages(service:selectedService)
        }
        cell2.AddServiceCallBack = {
            (quantity) -> () in
            self.addCurrentService(qty:quantity)
        }
        return cell2
    }
    private func showCurrentServiceImages(service:ServicesData) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowServiceImagesViewController") as! ShowServiceImagesViewController
//      navVC.images = service.image?.large
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
    private func addCurrentService(qty:Int){
        
        if serType == "outside" {
            userData.Instance.fetchUser()
            self.reservationIdentifier = userData.Instance.identifierInside ?? -1
            let shopID = userData.Instance.userSelectedShopId as! Int
            let reservationShopId = userData.Instance.currentReserveShopId as! Int
            if shopID != reservationShopId  {
                makeCannotReserveAlert1(title: "", SubTitle: "You cannot add any service or package from another shop you have to confirm the previous reservation or continue with a new one", Image: #imageLiteral(resourceName: "img34") , qty:qty)
            }else{
            reservationIdentifier = userData.Instance.identifierOutside ?? -1
            if reservationIdentifier == nil || reservationIdentifier == -1{
                PreparingReservationShopData(id:shopID, type: true)
            }else{
                PreparingReservationShopData(id:shopID, type: false)
            }
            userData.Instance.fetchUser()
            let userIdentifier = userData.Instance.identifierOutside
                self.AddService(serviceId: self.currentServiceID, userIdentifier: userIdentifier ?? -1 , serviceType: self.serType, quantity: qty)
            userData.Instance.removeserviceType()
            userData.Instance.serviceType = "outside"
            }
        }
        else{
            userData.Instance.fetchUser()
            self.reservationIdentifier = userData.Instance.identifierInside ?? -1
            let shopID = userData.Instance.userSelectedShopId as! Int
            let reservationShopId = userData.Instance.currentReserveShopId as! Int
            if shopID != reservationShopId  {
                makeCannotReserveAlert1(title: "", SubTitle: "You cannot add any service or package from another shop you have to confirm the previous reservation or continue with a new one", Image: #imageLiteral(resourceName: "img34"), qty: qty)
            }else{
            reservationIdentifier = userData.Instance.identifierInside ?? -1
            if reservationIdentifier == nil || reservationIdentifier == -1{
                PreparingReservationShopData(id:shopID, type: true)
            }else{
                PreparingReservationShopData(id:shopID, type: false)
            }
            userData.Instance.fetchUser()
            let userIdentifier = userData.Instance.identifierInside
                self.AddService(serviceId: self.currentServiceID, userIdentifier: userIdentifier ?? -1 , serviceType: self.serType, quantity: qty)
            userData.Instance.removeserviceType()
            userData.Instance.serviceType = "inside"
            }
        }
        print(userData.Instance.serviceType)
    }
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData(id:Int , type:Bool){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if self.serType == "outside" {
                            if let identifier = self.serviceReservation.data?.id {
                                userData.Instance.identifierOutside =  identifier
                                print(identifier)
                                UserDefaults.standard.set(identifier, forKey: "identifierOutside")
                            }
                            }else{
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierInside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier, forKey: "identifierInside")
                                }
                            }
                        }
                       
                        print(self.serType)
                     
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData1(id:Int , type:Bool){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if self.serType == "outside" {
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierOutside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier, forKey: "identifierOutside")
                                }
                            }else{
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierInside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier, forKey: "identifierInside")
                                }
                            }
                        }
                        
                        print(self.serType)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData11(id:Int , type:Bool, qty:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if self.serType == "outside" {
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierOutside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier, forKey: "identifierOutside")
                                        userData.Instance.fetchUser()
                                        self.reservationIdentifier = userData.Instance.identifierOutside ?? -1
                                    self.AddService(serviceId: self.currentServiceID, userIdentifier: self.reservationIdentifier ?? -1 , serviceType: self.serType, quantity: qty)
                                   
                                }
                            }else{
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierInside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier, forKey: "identifierInside")
                                    userData.Instance.fetchUser()
                                    self.reservationIdentifier = userData.Instance.identifierInside ?? -1
                                    self.AddService(serviceId: self.currentServiceID, userIdentifier: self.reservationIdentifier ?? -1 , serviceType: self.serType, quantity: qty)
                                }
                            }
                        }
                        
                        print(self.serType)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
 
    func AddService(serviceId:Int , userIdentifier:Int , serviceType:String , quantity:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        print(APIs.Instance.AddService(id: serviceId, identifier: userIdentifier, serviceType: serviceType, quantity: quantity))
        HUD.show(.progress, onView: self.view)
            print(APIs.Instance.AddService(id: serviceId, identifier: userIdentifier, serviceType: serviceType, quantity: quantity))
        Alamofire.request(APIs.Instance.AddService(id: serviceId, identifier: userIdentifier, serviceType: serType , quantity: quantity), method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print(temp)
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        userData.Instance.removeserviceType()
                        UserDefaults.standard.set(self.serType, forKey: "serviceType")
                        userData.Instance.removecurrentReserveShopId()
                        let shopID = userData.Instance.userSelectedShopId as! Int
                        UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func showAndHide(_ sender: Any) {
        tableView.reloadData()
    }
    
    @IBAction func insideOrOutSide(_ sender: Any) {
        loadShopData(id: currentID)
    }
    func makeCannotReserveAlert1(title: String, SubTitle: String, Image : UIImage , qty:Int) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.addButton("Go To Reservations") {
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController") as! Cart1ViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }
        alert.addButton("Continue Current Reservations") {
            userData.Instance.fetchUser()
            if userData.Instance.serviceType == "inside" {
                userData.Instance.removeIdentifierInside()
                let shopID = userData.Instance.userSelectedShopId as! Int
                UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                self.PreparingReservationShopData11(id:shopID, type: true , qty:qty)
            }else{
                userData.Instance.removeidentifierOutside()
                let shopID = userData.Instance.userSelectedShopId as! Int
                UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                self.PreparingReservationShopData11(id:shopID, type: true, qty:qty)
            }
        }
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Dismiss", comment: "إلغاء"), andButtons: nil)
    }
 
}
