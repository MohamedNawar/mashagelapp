//
//  CustomerOpinionsInMainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import HCSStarRatingView
class CustomerOpinionsInMainViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    //    var id = Int(){
    //        didSet{
    //            print(id)
    //          loadShopReviewsData(id: id)
    //          loadShopData(id: id)
    //        }
    //    }

    var shopData = Shop(){
        didSet{
            rate.value = CGFloat(shopData.data?.review_count ?? 0)
            var rateValue = shopData.data?.review_average ?? ""
            if let n = NumberFormatter().number(from: rateValue) {
                let f = CGFloat(n)
                rateLbl.text = "\(f)"
            }
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var rate: HCSStarRatingView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        userData.Instance.fetchUser()
        print("eeeeeee")
        let currentID = userData.Instance.userSelectedShopId as! Int
        print(userData.Instance.userSelectedShopId)
  //      loadShopData(id: currentID)
        self.title = NSLocalizedString("Reviews", comment: "أراء العملا")
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopData.data?.review?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerOpinionsCell", for: indexPath)
        if let nameValue = shopData.data?.review?[indexPath.row].name{
            let name = cell.viewWithTag(1) as! UILabel
            name.text = "\(nameValue)"
        }
        if let messageValue = shopData.data?.review?[indexPath.row].message {
            let message = cell.viewWithTag(2) as! UILabel
            message.text = "\(messageValue ?? "")"
        }
        if let rateValue = shopData.data?.review?[indexPath.row].review {
            let ratting = cell.viewWithTag(10) as! HCSStarRatingView
            ratting.value = CGFloat(rateValue)
        }
        if let dateValue = shopData.data?.review?[indexPath.row].created_at?.date {
            let date = cell.viewWithTag(8) as! UILabel
            var stringValue = dateValue
            stringValue = stringValue.components(separatedBy: " ")[0]
            date.text = "\(stringValue)"
        }
        //        if let  imageIndex = (reviewData.data?[indexPath.row].image?.large) {
        //            let image = cell.viewWithTag(5) as! UIImageView
        //            image.sd_setImage(with: URL(string: imageIndex), completed: nil)
        //        }
        
        return cell
    }
  /*  func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    } */

    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
