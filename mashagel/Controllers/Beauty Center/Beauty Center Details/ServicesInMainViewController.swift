//
//  ServicesInMainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
protocol ChooseShopDelegateForService {
    func userChooseAShopForService(shopId: Int)
}
class ServicesInMainViewController:UIViewController , UITableViewDelegate , UITableViewDataSource {
    
     // TODO: - PASS DATA
    var shopData = ShopData()
    // MARK: -  variables
    var packageId = Int()
    var reservationIdentifier = Int()
    var id = Int()
    var packageReservation = Reservation()
    var currentPackage = packagesData()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = false
        userData.Instance.fetchUser()
        print("eeeeeee")
        print(userData.Instance.userSelectedShopId)
        let currentID = userData.Instance.userSelectedShopId as! Int
        
        // TODO: - REMOVE API CALL
        print(" get ==> ServiceVC ==> service ")
        //loadShopData(id: currentID)
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
        self.title = NSLocalizedString("Offers", comment: "العروض")
        let shopID = userData.Instance.userSelectedShopId as! Int
        let reservationShopId = userData.Instance.currentReserveShopId ?? -1
        reservationIdentifier = userData.Instance.identifierInside ?? -1
        if reservationIdentifier == nil || reservationIdentifier == -1{
            PreparingReservationShopData(id:shopID, type: true)
        }else{
            PreparingReservationShopData(id:shopID, type: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return shopData.packages?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = shopData.packages?[section].services?.count ?? 0
        return count+2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderServicesCell", for: indexPath)
            if let nameValue = shopData.packages?[indexPath.section].name{
                let name = cell.viewWithTag(2) as! UILabel
                name.text = "\(nameValue)"
                if L102Language.currentAppleLanguage() == "ar" {
                    name.textAlignment = .right
                }
            }
            if let priceValue = shopData.packages?[indexPath.section].price{
                let price = cell.viewWithTag(3) as! UILabel
                if let n = NumberFormatter().number(from: priceValue) {
                    let f = Int(n)
                    price.text = "$\(f)"
                }
                
            }
            if let imageValue = shopData.packages?[indexPath.section].image?.first?.large{
                let image = cell.viewWithTag(1) as! UIImageView
                image.sd_setImage(with: URL(string: imageValue), completed: nil)
            }
            
            return cell
        }
        let count = (shopData.packages?[indexPath.section].services?.count) ?? 0
        if indexPath.row == count+1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterServicesCell", for: indexPath) as! PackageReservationCell
            packageId =  shopData.packages?[indexPath.section].id ?? 0
            print(packageId)
            cell.PackageReservationCallBack = {
                    self.addCurrentPackage()
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell", for: indexPath)
        if let titleValue = shopData.packages?[indexPath.section].services?[indexPath.row-1].name {
            let title = cell.viewWithTag(7) as! UILabel
            title.text = "\(titleValue)"
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if L102Language.currentAppleLanguage() == "en" {
                title.textAlignment = .left
            }
        }
        
        return cell
    }
    private func addCurrentPackage(){
        userData.Instance.fetchUser()
        self.reservationIdentifier = userData.Instance.identifierInside ?? -1
        print(self.reservationIdentifier)
        let shopID = userData.Instance.userSelectedShopId ?? -1
        let reservationShopId = userData.Instance.currentReserveShopId ?? -1
        print(shopID)
        print(reservationShopId)
        if shopID != reservationShopId && reservationShopId != -1 {
            userData.Instance.fetchUser()
         
            makeCannotReserveAlert(title: "", SubTitle: "You cannot add any service or package from another shop you have to confirm the previous reservation or continue with a new one", Image: #imageLiteral(resourceName: "img34"))
            
        }else{
        self.AddPackage(packageId: self.packageId, userIdentifier: self.reservationIdentifier )
        }
    }
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                     //   self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData(id:Int , type:Bool){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.packageReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if let identifier = self.packageReservation.data?.id {
                                userData.Instance.identifierInside =  identifier
                                print(identifier)
                                userData.Instance.removeIdentifierInside()
                                UserDefaults.standard.set(identifier, forKey: "identifierInside")
                            }
                        }
                   
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData1(id:Int , type:Bool){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.packageReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if let identifier = self.packageReservation.data?.id {
                                userData.Instance.identifierInside =  identifier
                                print(identifier)
                                userData.Instance.removeIdentifierInside()
                                UserDefaults.standard.set(identifier, forKey: "identifierInside")
                                userData.Instance.fetchUser()
                                self.reservationIdentifier = userData.Instance.identifierInside ?? -1
                                print(self.reservationIdentifier)
                                self.AddPackage(packageId: self.packageId, userIdentifier: self.reservationIdentifier )
                            }
                        }
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func AddPackage(packageId:Int , userIdentifier:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.AddPackage(id: packageId, identifier: userIdentifier))
        Alamofire.request(APIs.Instance.AddPackage(id: packageId, identifier: userIdentifier) , method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                        userData.Instance.removeserviceType()
                         UserDefaults.standard.set("inside", forKey: "serviceType")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.packageReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        userData.Instance.removeserviceType()
                        UserDefaults.standard.set("inside", forKey: "serviceType")
                        userData.Instance.fetchUser()
                        let shopID = userData.Instance.userSelectedShopId as! Int
                         UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    func makeCannotReserveAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.addButton("Go To Reservations") {
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController") as! Cart1ViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }
        alert.addButton("Continue Current Reservations") {
            if userData.Instance.serviceType == "inside" {
                userData.Instance.removeIdentifierInside()
                let shopID = userData.Instance.userSelectedShopId as! Int
                UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                self.PreparingReservationShopData1(id:shopID, type: true)
            }
            
            
        }
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Dismiss", comment: "إلغاء"), andButtons: nil)
    }
    
}

