//
//  OffersInMainViewController1.swift
//  mashagel
//
//  Created by ElNoorOnline on 1/6/19.
//  Copyright © 2019 MohamedHassanNawar. All rights reserved.
//

import UIKit

struct cellData {
    var opened = Bool()
    var title = String()
}

class OffersInMainViewController1: UIViewController , UITableViewDelegate, UITableViewDataSource, TableViewCellDelegate {
    

    
    @IBOutlet weak var tableView: UITableView!
    var user_arr  = [cellData]()
    
    var shopData = ShopData(){
        didSet{
            for item in shopData.services! {
                
                
                if new_shopData.keys.contains(item.category?.name ?? "") {
                    new_shopData[(item.category?.name)!]?.append(item)
                    
                }else{
                    new_shopData[(item.category?.name)!] = [item]
                }
                
            }
            
            //      print("-----\(new_shopData.count)---\(new_shopData.keys)")
        }
    }
    var new_shopData : [String: [ServicesData]] = [:]
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // TODO: - REMOVE API CALL
        print(" get ==> OffersVC ==> service ")
        
        user_arr = [cellData(opened: false, title: "aa"),
                    cellData(opened: false, title: "oo"),
                    cellData(opened: false, title: "uu")
        ]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()

        
    }
    
    
    @IBAction func switch_service(_ sender: Any) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return new_shopData.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if user_arr[section].opened {
            print("****\(Array(new_shopData)[section].value.count)")
            return Array(new_shopData)[section].value.count + 1
        }else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            
            let cell = Bundle.main.loadNibNamed("OfferSectionHeader", owner: self, options: nil)?.first as! OfferSectionHeader
            
          //  cell.configure_cell(data: shopData.services?[indexPath.section] ?? ServicesData())
            
            let category_name = Array(new_shopData)[indexPath.section].key
            cell.configure_cell(data: category_name)

            
            let services = shopData.services
            return cell
            
        }else{
            let cell = Bundle.main.loadNibNamed("OfferCollapseCell", owner: self, options: nil)?.first as! OfferCollapseCell
            
        //    cell.configure_cell(data: shopData.services?[indexPath.section] ?? ServicesData())
            
            let category_name = Array(new_shopData)[indexPath.section].value[indexPath.row - 1]
            cell.delegate = self
//            cell.tag = Array(new_shopData)[indexPath.section].value[indexPath.row - 1].id ?? -1
//            cell.addService_btn.addTarget(self, action: #selector(addService), for: .touchUpInside)
            cell.configure_cell(data: category_name)

            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 44
        }else {
            
            return 240
        }
        
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //  let cartVC = self.storyboard?.instantiateViewController(withIdentifier: "zz") as! GymDetails
        //self.navigationController?.pushViewController(cartVC, animated: true)
        
        //  let go = ViewController()
        //go.go1()
        
        //    self.performSegue(withIdentifier: "goToGymDetails", sender: self)
        //        let go = GymDetails()
        //  go.getCellPresses(num: indexPath.row)
   //     tableView.deselectRow(at: indexPath, animated: true)
        //    self.navigationController?.pushViewController(go, animated: true)
        
        
        if indexPath.row == 0 {

            user_arr[indexPath.section].opened = !user_arr[indexPath.section].opened
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        
        }
    }
    
    // MARK: - add btn
    func didAddService(_ sender: OfferCollapseCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        
        let cell1 =  tableView.cellForRow(at: tappedIndexPath) as! OfferCollapseCell
        
        print("+++++\(tappedIndexPath.row)++++\(tappedIndexPath.section)++++\(cell1.numOfPerson_lbl.text!)")
    }
    
    @objc func addService(_ sender: UIButton){
 
    }
    
    
    
}
