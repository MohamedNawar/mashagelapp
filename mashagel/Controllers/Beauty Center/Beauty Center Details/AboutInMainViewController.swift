//
//  AboutInMainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
class AboutInMainViewController:  UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    var shopData = Shop(){
        didSet{
             let name = tableView.viewWithTag(110) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                name.textAlignment = .right
            }
            let name1 = tableView.viewWithTag(1010) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                name1.textAlignment = .right
            }
            let title = tableView.viewWithTag(44) as! UILabel
            title.text = shopData.data?.address
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            let title1 = tableView.viewWithTag(45) as! UILabel
            title1.text = shopData.data?.mobile
            if L102Language.currentAppleLanguage() == "ar" {
                title1.textAlignment = .right
            }
            self.tableView.reloadData()
            self.title = NSLocalizedString("About Shop", comment: "عن المحل")
            
        }
    }
    //    var id = Int(){
    //        didSet{
    //            print(id)
    //             loadShopData(id: id)
    //
    //        }
    //    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(shopData.data?.schedule?.count ?? 0)
        return shopData.data?.schedule?.count ?? 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell", for: indexPath)
        if let titleValue = shopData.data?.schedule?[indexPath.row].day_of_week {
            let title = cell.viewWithTag(1) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            switch titleValue {
            case 1:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "السبت"
                }else{
                    title.text = NSLocalizedString("Saturday", comment:"السبت")
                }
            case 2:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأحد"
                }else{
                    title.text = NSLocalizedString("Sunday", comment:"الأحد")
                }
            case 3:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأثنين"
                }else{
                    title.text = NSLocalizedString("Monday", comment:"الأثنين")
                }
            case 4:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الثلاثاء"
                }else{
                    title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                }
            case 5:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأربعاء"
                }else{
                    title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                }
            case 6:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الخميس"
                }else{
                    title.text = NSLocalizedString("Thursday", comment:"الخميس")
                }
            case 7:
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الجمعة"
                }else{
                    title.text = NSLocalizedString("Friday", comment:"الجمعة")
                }
            default:
                title.text = "\(titleValue)"
            }
        }
        if let titleValue = shopData.data?.schedule?[indexPath.row] {
            let title = cell.viewWithTag(3) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if var from = titleValue.from_inside{
                if var to = titleValue.to_inside{
                   var toCount = to.count
                  
                    to.remove(at: String.Index(encodedOffset: toCount))
                    to.remove(at: String.Index(encodedOffset: toCount-1))
                    to.remove(at: String.Index(encodedOffset: toCount-2))
                    var fromCount = from.count
                    from.remove(at: String.Index(encodedOffset: fromCount))
                    from.remove(at: String.Index(encodedOffset: fromCount-1))
                    from.remove(at: String.Index(encodedOffset: fromCount-2))
                    from.characters.removeLast()
                    to.characters.removeLast()
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text =
                             "من الساعة\(from) صباحاإلي الساعة\(to) ليلا داخل الصالون"
                    }else{
                           NSLocalizedString("from \(from) to \(to) inside Salon", comment:   "من الساعة\(from)  صباحاإلي الساعة\(to) ليلا داخل الصالون")
                    }
                                     }
            }
        }
        if let titleValue1 = shopData.data?.schedule?[indexPath.row] {
            let title = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if var from = titleValue1.from_outside{
                if var to = titleValue1.to_outside{
                    var toCount = to.count
                    to.remove(at: String.Index(encodedOffset: toCount))
                    to.remove(at: String.Index(encodedOffset: toCount-1))
                    to.remove(at: String.Index(encodedOffset: toCount-2))
                    var fromCount = from.count
                    from.remove(at: String.Index(encodedOffset: fromCount))
                    from.remove(at: String.Index(encodedOffset: fromCount-1))
                    from.remove(at: String.Index(encodedOffset: fromCount-2))
                    from.characters.removeLast()
                    to.characters.removeLast()
                    if L102Language.currentAppleLanguage() == "ar"{
                       title.text =  "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون"                    }else{
                        title.text = NSLocalizedString("from \(from) to \(to) outside Salon", comment:   "من الساعة\(from) صباحاإلي الساعة\(to) ليلا خارج الصالون")
                    }
                 
                }
            }
        }
        
        return cell
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = false
        userData.Instance.fetchUser()
        print("eeeeeee")
        let currentID = userData.Instance.userSelectedShopId as! Int
        print(userData.Instance.userSelectedShopId)
        
 //       loadShopData(id: currentID)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: .myNotificationKey, object: nil)
        
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
        
    }
    //    @objc func notificationReceived(_ notification: Notification) {
    //        guard let shopData1 = notification.userInfo?["shopData"] as? Shop else { return }
    //        print ("qqqqqqqqqq")
    //        print(shopData1)
    //        shopData = shopData1
    //    }
//    func loadShopData(id:Int){
//        let header = APIs.Instance.getHeader()
//        HUD.show(.progress, onView: self.view)
//        print(APIs.Instance.showShop(id:id))
//        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//                print(value)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
//                        print(err.errors)
//                    }catch{
//                        print("errorrrrelse")
//                    }
//                }else{
//                    do {
//                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
//
//                    }catch{
//                        print(error)
//                        HUD.flash(.label("Error Try Again"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
//                break
//            }
//        }
//    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
