//
//  MainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import FCAlertView
import SJSegmentedScrollView
import Alamofire

class MainViewController: SJSegmentedViewController {
    var shopTitle = String()
    var selectedSegment: SJSegmentTab?
    var shopData = ShopData()
    var id = Int(){
        didSet{
      //test
            print(id)
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        userData.Instance.fetchUser()
        print("eeeeeee")
         let currentID = userData.Instance.userSelectedShopId as! Int
      //  loadShopData(id: currentID)

    }
    override func viewDidLoad() {
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        
        setUp_segment()
        
        title = shopTitle
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    // MARK: - SETUP SEGMENT
    func setUp_segment()  {
        if let storyboard = self.storyboard {
            let headerController = storyboard
                .instantiateViewController(withIdentifier: "HeaderViewController1") as! HeaderViewController
            headerController.shopData = shopData
            
            
            let firstViewController = storyboard
                .instantiateViewController(withIdentifier: "ServicesInMainViewController") as? ServicesInMainViewController
            firstViewController?.title = "Offers"
            firstViewController?.shopData = shopData
            
            let secondViewController = storyboard
                .instantiateViewController(withIdentifier: "OffersInMainViewController1") as? OffersInMainViewController1
            secondViewController?.title = "Services"
            secondViewController?.shopData = shopData
            
            let thirdViewController = storyboard
                .instantiateViewController(withIdentifier: "AboutInMainViewController") as? AboutInMainViewController
            thirdViewController?.title = "About Center"
            let fourthViewController = storyboard
                .instantiateViewController(withIdentifier: "CustomerOpinionsInMainViewController") as? CustomerOpinionsInMainViewController
            fourthViewController?.title = "Reviews"
            headerViewController = headerController
            segmentControllers = [firstViewController,
                                  secondViewController,
                                  thirdViewController,
                                  fourthViewController] as! [UIViewController]
            headerViewHeight = 250
            selectedSegmentViewHeight = 5.0
            headerViewOffsetHeight = 0.0
            segmentTitleColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
            selectedSegmentViewColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
            segmentShadow = SJShadow.light()
            showsHorizontalScrollIndicator = false
            showsVerticalScrollIndicator = false
            segmentBounces = false
            delegate = self
        }
    }
    func getSegmentTabWithImage(_ imageName: String) -> UIView {
        
        let view = UIImageView()
        view.frame.size.width = 100
        view.image = UIImage(named: imageName)
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .white
        return view
    }
}

extension MainViewController: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        if selectedSegment != nil {
            selectedSegment?.titleColor(.lightGray)
        }
        
        if segments.count > 0 {
            
            selectedSegment = segments[index]
            selectedSegment?.titleColor(.red)
        }
    }
}




