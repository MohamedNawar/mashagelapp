//
//  AddEmployeeScheduleViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/8/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
class AddEmployeeScheduleViewController: UIViewController {
    var id = Int(){
        didSet{
            print(id)
        }
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    let saturdayFromPicker = UIDatePicker()
    let saturdayToPicker = UIDatePicker()
    let sundayFromPicker = UIDatePicker()
    let sundayToPicker = UIDatePicker()
    let mondayFromPicker = UIDatePicker()
    let mondayToPicker = UIDatePicker()
    let tuesdayFromPicker = UIDatePicker()
    let tuesdayToPicker = UIDatePicker()
    let wednesdayFromPicker = UIDatePicker()
    let wednesdayToPicker = UIDatePicker()
    let thursdayFromPicker = UIDatePicker()
    let thursdayToPicker = UIDatePicker()
    let fridayFromPicker = UIDatePicker()
    let fridayToPicker = UIDatePicker()
    @IBOutlet weak var fridayTo: HoshiTextField!
    @IBOutlet weak var fridayFrom: HoshiTextField!
    @IBOutlet weak var thursdayTo: HoshiTextField!
    @IBOutlet weak var thursdayFrom: HoshiTextField!
    @IBOutlet weak var wednesdayTo: HoshiTextField!
    @IBOutlet weak var wednesdayFrom: HoshiTextField!
    @IBOutlet weak var tuesdayTo: HoshiTextField!
    @IBOutlet weak var tuesdayFrom: HoshiTextField!
    @IBOutlet weak var mondayTo: HoshiTextField!
    @IBOutlet weak var mondayFrom: HoshiTextField!
    @IBOutlet weak var sundayTo: HoshiTextField!
    @IBOutlet weak var sundayFrom: HoshiTextField!
    @IBOutlet weak var saturdayTo: HoshiTextField!
    @IBOutlet weak var saturdayFrom: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        // Do any additional setup after loading the view.
        saturdayFromPicker.backgroundColor = .white
        saturdayToPicker.backgroundColor = .white
        sundayFromPicker.backgroundColor = .white
        sundayToPicker.backgroundColor = .white
        mondayFromPicker.backgroundColor = .white
        mondayToPicker.backgroundColor = .white
        tuesdayFromPicker.backgroundColor = .white
        tuesdayToPicker.backgroundColor = .white
        wednesdayFromPicker.backgroundColor = .white
        wednesdayToPicker.backgroundColor = .white
        thursdayFromPicker.backgroundColor = .white
        thursdayToPicker.backgroundColor = .white
        fridayFromPicker.backgroundColor = .white
        fridayToPicker.backgroundColor = .white
        self.title = NSLocalizedString("Add Employee schedule", comment: "أضافة جدول الموظف")
        showsaturdayFromPicker()
        showsaturdayToPicker()
        showSundayFromPicker()
        showsundayToPicker()
        showMondayFromPicker()
        showMondayToPicker()
        showTuesdayFromPicker()
        showTuesdayToPicker()
        showWednesdayFromPicker()
        showWednesdayToPicker()
        showThursdayFromPicker()
        showThursdayToPicker()
        showFridayFromPicker()
        showFridayToPicker()
    }
    func showsaturdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        saturdayFrom.inputAccessoryView = toolbar
        saturdayFrom.inputView = saturdayFromPicker
        
    }
    func showsaturdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker1));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        saturdayTo.inputAccessoryView = toolbar
        saturdayTo.inputView = saturdayToPicker
        
    }
    //
    func showSundayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker2));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        sundayFrom.inputAccessoryView = toolbar
        sundayFrom.inputView = sundayFromPicker
        
    }
    func showsundayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker3));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        sundayTo.inputAccessoryView = toolbar
        sundayTo.inputView = sundayToPicker
        
    }
    //
    func showMondayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker4));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        mondayFrom.inputAccessoryView = toolbar
        mondayFrom.inputView = mondayFromPicker
        
    }
    func showMondayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker5));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        mondayTo.inputAccessoryView = toolbar
        mondayTo.inputView = mondayToPicker
        
    }
    //
    func showTuesdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker6));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tuesdayFrom.inputAccessoryView = toolbar
        tuesdayFrom.inputView = tuesdayFromPicker
        
    }
    func showTuesdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker7));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tuesdayTo.inputAccessoryView = toolbar
        tuesdayTo.inputView = tuesdayToPicker
        
    }
    //
    func showWednesdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker8));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        wednesdayFrom.inputAccessoryView = toolbar
        wednesdayFrom.inputView = wednesdayFromPicker
        
    }
    func showWednesdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker9));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        wednesdayTo.inputAccessoryView = toolbar
        wednesdayTo.inputView = wednesdayToPicker
        
    }
    //
    func showThursdayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker10));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        thursdayFrom.inputAccessoryView = toolbar
        thursdayFrom.inputView = thursdayFromPicker
        
    }
    func showThursdayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        thursdayTo.inputAccessoryView = toolbar
        thursdayTo.inputView = thursdayToPicker
        
    }
    //
    func showFridayFromPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker12));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fridayFrom.inputAccessoryView = toolbar
        fridayFrom.inputView = fridayFromPicker
        
    }
    func showFridayToPicker(){
        //Formate Date
        saturdayFromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker13));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        fridayTo.inputAccessoryView = toolbar
        fridayTo.inputView = fridayToPicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        saturdayFrom.text = formatter.string(from: saturdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func donedatePicker1(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        saturdayTo.text = formatter.string(from: saturdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker2(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        sundayFrom.text = formatter.string(from: sundayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker3(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        sundayTo.text = formatter.string(from: sundayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker4(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        mondayFrom.text = formatter.string(from: mondayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker5(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        mondayTo.text = formatter.string(from: mondayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker6(){
        let formatter = DateFormatter()
        formatter.dateFormat = "H:M:S"
        tuesdayFrom.text = formatter.string(from: tuesdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker7(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        tuesdayTo.text = formatter.string(from: tuesdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker8(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        wednesdayFrom.text = formatter.string(from: wednesdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker9(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        wednesdayTo.text = formatter.string(from: wednesdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker10(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        thursdayFrom.text = formatter.string(from: thursdayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker11(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        thursdayTo.text = formatter.string(from: thursdayToPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker12(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fridayFrom.text = formatter.string(from: fridayFromPicker.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker13(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        fridayTo.text = formatter.string(from: fridayToPicker.date)
        self.view.endEditing(true)
    }
    @IBAction func addSchedule(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.saturdayFrom.text != "" && self.saturdayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.saturdayFrom.text ?? "", to: self.saturdayTo.text ?? "" , day: 1)
            }
            if self.sundayFrom.text != "" && self.sundayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.sundayFrom.text ?? "", to: self.sundayTo.text ?? "", day: 2)
            }
            if self.mondayFrom.text != "" && self.mondayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.mondayFrom.text ?? "", to: self.mondayTo.text ?? "", day: 3)
            }
            if self.tuesdayFrom.text != "" && self.tuesdayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.tuesdayFrom.text ?? "", to: self.tuesdayTo.text ?? "", day:4)
            }
            if self.wednesdayFrom.text != "" && self.wednesdayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.wednesdayFrom.text ?? "", to: self.wednesdayTo.text ?? "", day: 5)
            }
            if self.thursdayFrom.text != "" && self.thursdayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.thursdayFrom.text ?? "", to: self.thursdayTo.text ?? "", day: 6)
            }
            if self.fridayFrom.text != "" && self.fridayTo.text != "" {
                self.PostShopSchedule(id: self.id, from: self.fridayFrom.text ?? "", to: self.fridayTo.text ?? "", day: 7)
            }
        }
    }
    func PostShopSchedule(id: Int , from:String , to:String , day:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        let par = ["from": from , "to": to , "day_of_week":day] as [String : Any]
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddEmployeeSchedule(id: 5), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors?.password)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print("successsss")
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
