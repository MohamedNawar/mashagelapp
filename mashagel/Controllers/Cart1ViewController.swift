//
//  Cart1ViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//'
import SideMenu
import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
import SideMenu
class Cart1ViewController: UIViewController , UITextFieldDelegate ,  UITableViewDelegate , UITableViewDataSource,UIPickerViewDataSource , UIPickerViewDelegate  {
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBOutlet weak var topDistanceToview: NSLayoutConstraint!
    @IBOutlet weak var heightForDateTextfield: NSLayoutConstraint!
    @IBOutlet weak var headerHeightForView: NSLayoutConstraint!
    var currentService = ServicesData()
    var currentPackage = Package_item()
    var headerHeight = Int()
    var sectionsCount = Int()
    let datePicker = UIDatePicker()
    var reserVation1Id = Int()
    var reservation1 = Reservation(){
        didSet{
            if let dateValue = reservation1.data?.date?.date {
                var stringValue = dateValue
                stringValue = stringValue.components(separatedBy: " ")[0]
                dateTextField.text = stringValue
            }else{
                if let date = userData.Instance.selectedDate {
                    dateTextField.text = date
                }
            }
            if let priceValue = reservation1.data?.total_price{
                totalPriceLabl.text = "$\(priceValue)"
            }
            
            self.tableView.reloadData()
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == timePicker {
            return "\(words2[row])"
            
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return words2.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == timePicker {
            selectedItem2 = words2[row]
            dateTextField1.text = selectedItem2
        }
    }
    var reservationForTime = ReservationForTime()
        var reservation = Reservation(){
            didSet{
                if let dateValue = reservation.data?.date?.date {
                    var stringValue = dateValue
                    stringValue = stringValue.components(separatedBy: " ")[0]
                    dateTextField.text = stringValue
                }else{
                    if let date = userData.Instance.selectedDate {
                        dateTextField.text = date
                    }
                }
                if let priceValue = reservation.data?.total_price{
                    totalPriceLabl.text = "$\(priceValue)"
                }
                self.words2 = self.reservationForTime.data?.times ?? [String]()
                if self.words2.count <= 0 {
                    self.dateTextField1.isEnabled = false
                    self.dateTextField1.isUserInteractionEnabled = false
                }else{
                    self.dateTextField1.isEnabled = true
                    self.dateTextField1.isUserInteractionEnabled = true
                }
                self.tableView.reloadData()
            }
        }
        let timePicker = UIPickerView()
        var words2 = [String]()
        var selectedItem2 = ""
        var reserVationId = Int()
        @IBOutlet weak var hiddentextField: UITextField!
    @IBOutlet weak var hiddentextField1: UITextField!
        @IBOutlet weak var dateTextField: DesignableTextField!
       @IBOutlet weak var dateTextField1: DesignableTextField!
        @IBOutlet weak var tableView: UITableView!
        @IBOutlet weak var totalPriceLabl: UILabel!
        @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var buttonDistance: NSLayoutConstraint!
    @IBOutlet weak var buttonDistance2: NSLayoutConstraint!
    
        override func viewDidLoad() {
            super.viewDidLoad()
            setupSideMenu()
            hiddentextField1.isEnabled = false
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
            timePicker.backgroundColor = .white
            timePicker.delegate = self
            timePicker.dataSource = self
            timePicker.showsSelectionIndicator = true
            self.title = NSLocalizedString("Reservations", comment: "الحجوزات")
            navigationItem.title = NSLocalizedString("Reservations", comment: "الحجوزات")
            userData.Instance.fetchUser()
            tableView.allowsSelection = false
            datePicker.backgroundColor = .white
            tableView.separatorStyle = .none
            tableView.allowsSelection = false
            hiddentextField.delegate = self
               hiddentextField1.delegate = self
            userData.Instance.fetchUser()
        hiddentextField.addPadding(UITextField.PaddingSide.left(20))
    hiddentextField1.addPadding(UITextField.PaddingSide.left(20))
            hiddentextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            hiddentextField1.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            showDatePicker()
            showTimePicker()
            tabBarItem.imageInsets.top = -6
            if userData.Instance.serviceType == "inside"{
                headerHeightForView.constant = 0
                heightForDateTextfield.constant = 40
                buttonDistance.constant = 20
                buttonDistance2.constant = 20
                topDistanceToview.constant = 20
                reserVationId = userData.Instance.identifierInside ?? -1
                dateTextField1.isHidden = true
                dateTextField1.isEnabled = false
                hiddentextField1.isHidden = true
                if dateTextField.text != "" {
                    if userData.Instance.serviceType == "inside"{
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                        PreparingSendDateForCurrentReservation(id:reserVationId)
         }else{
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                        PreparingSendDateForCurrentReservation1(id:reserVationId)
          }
                }
                if let dateValue = reservation.data?.date?.date {
                    var stringValue = dateValue
                    stringValue = stringValue.components(separatedBy: " ")[0]
                    dateTextField.text = stringValue
                }else{
                    if let date = userData.Instance.selectedDate {
                        dateTextField.text = date
                        if userData.Instance.serviceType == "inside"{
                            userData.Instance.fetchUser()
                            self.reserVation1Id = userData.Instance.identifierInside ?? -1
                            PreparingSendDateForCurrentReservation(id:reserVationId)
                        }else{
                                  userData.Instance.fetchUser()
                            self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                            PreparingSendDateForCurrentReservation1(id:reserVationId)
                        }                    }
                }
            }else{
                headerHeightForView.constant = CGFloat(40)
                heightForDateTextfield.constant = 40
                buttonDistance.constant = 0
                buttonDistance2.constant = 0
                topDistanceToview.constant = 0
                reserVationId = userData.Instance.identifierOutside ?? -1
                dateTextField1.isHidden = false
                dateTextField1.isEnabled = true
                hiddentextField1.isHidden = false
                if dateTextField.text != "" {
                    if userData.Instance.serviceType == "inside"{
                              userData.Instance.fetchUser()
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                        PreparingSendDateForCurrentReservation(id:reserVation1Id)
                    }else{
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                        PreparingSendDateForCurrentReservation1(id:reserVation1Id)
                    }                }
                if let dateValue = reservation.data?.date?.date {
                    var stringValue = dateValue
                    stringValue = stringValue.components(separatedBy: " ")[0]
                    dateTextField.text = stringValue
                }else{
                    if let date = userData.Instance.selectedDate {
                        dateTextField.text = date
                        if userData.Instance.serviceType == "inside"{
                            userData.Instance.fetchUser()
                            self.reserVation1Id = userData.Instance.identifierInside ?? -1
                            PreparingSendDateForCurrentReservation(id:reserVation1Id)
                        }else{
                            self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                            PreparingSendDateForCurrentReservation1(id:reserVation1Id)
                        }                      }
                }
                if userData.Instance.serviceType == "inside"{
                    userData.Instance.fetchUser()
                    self.reserVation1Id = userData.Instance.identifierInside ?? -1
                    PreparingReservationShopData1(id: userData.Instance.identifierInside ?? -1)
                }else{
                    self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                    PreparingReservationShopData1(id: userData.Instance.identifierOutside ?? -1)
                }
            }

        }
    @IBAction func menue(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "en" {
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
            
        }else{
            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
            self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
             hiddentextField1.isEnabled = false
            if L102Language.currentAppleLanguage() == "ar"{
                self.title = "الحجوزات"
                navigationItem.title =  "الحجوزات"
            }else{
                self.title = NSLocalizedString("Reservations", comment: "الحجوزات")
                navigationItem.title = NSLocalizedString("Reservations", comment: "الحجوزات")
            }
            userData.Instance.fetchUser()
            if userData.Instance.serviceType == "inside"{
                headerHeightForView.constant = 0
                heightForDateTextfield.constant = 40
                buttonDistance.constant = 20
                buttonDistance2.constant = 20
                topDistanceToview.constant = 20
                reserVationId = userData.Instance.identifierInside ?? -1
                dateTextField1.isHidden = true
                dateTextField1.isEnabled = false
                hiddentextField1.isHidden = true
                
            }else{
                headerHeightForView.constant = CGFloat(40)
                heightForDateTextfield.constant = 40
                buttonDistance.constant = 0
                buttonDistance2.constant = 0
                topDistanceToview.constant = 0
                reserVationId = userData.Instance.identifierOutside ?? -1
                dateTextField1.isHidden = false
                dateTextField1.isEnabled = true
                hiddentextField1.isHidden = false
            }
            if userData.Instance.serviceType == "inside"{
                reserVation1Id = userData.Instance.identifierInside ?? -1
                PreparingSendDateForCurrentReservation(id:reserVation1Id)
            }else{
                reserVation1Id = userData.Instance.identifierOutside ?? -1
                PreparingSendDateForCurrentReservation1(id:reserVation1Id)
            }

            if L102Language.currentAppleLanguage() == "ar" {
                image.image = UIImage(named: "img02")
            }
        }
    func showDatePicker11(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title:NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker11));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    @objc func donedatePicker11(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        if userData.Instance.serviceType == "inside"{
            reserVation1Id = userData.Instance.identifierInside ?? -1
            PreparingSendDateForCurrentReservation(id:reserVation1Id)
        }else{
            reserVation1Id = userData.Instance.identifierOutside ?? -1
            PreparingSendDateForCurrentReservation1(id:reserVation1Id)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker11(){
        self.view.endEditing(true)
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        if userData.Instance.serviceType == "inside"{
            reserVation1Id = userData.Instance.identifierInside ?? -1
            PreparingSendDateForCurrentReservation(id:reserVation1Id)
        }else{
            reserVation1Id = userData.Instance.identifierOutside ?? -1
            PreparingSendDateForCurrentReservation1(id:reserVation1Id)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func showTimePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancelDatePicker1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dateTextField1.inputView = timePicker
        dateTextField1.inputAccessoryView = toolbar
    }
    @objc func donedatePicker1(){
        if dateTextField1.text == "" {
            if words2.count > 0 {
                dateTextField1.text = words2[0] ?? ""
                selectedItem2 = words2[0]
                if userData.Instance.serviceType == "inside"{
                    reserVation1Id = userData.Instance.identifierInside ?? -1
  self.SetEmployeeTimer(itemId:currentService.id ?? -1, time : selectedItem2, identifierId: reserVationId)                }else{
                    reserVation1Id = userData.Instance.identifierOutside ?? -1
  self.SetEmployeeTimer1(itemId:currentService.id ?? -1, time : selectedItem2, identifierId: reserVationId)                }
              
                self.view.endEditing(true)
            }
        }
     
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker1(){
        dateTextField1.text = ""
        self.view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       if userData.Instance.serviceType == "inside" {
            let service = reservation1.data?.service_items?.items?.count ?? 0
            if service > 0 {
                let packageCount = reservation1.data?.service_items?.package_items?.count
                return (packageCount ?? 0)+1
            }else{
                return reservation1.data?.service_items?.package_items?.count ?? 0
            }
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if userData.Instance.serviceType == "inside" {
            let service = reservation1.data?.service_items?.items?.count ?? 0
            if service > 0 {
                if section == reservation1.data?.service_items?.package_items?.count {
                    return service
                }
                return reservation1.data?.service_items?.package_items?[section].items?.count ?? 0
            }else{
                return reservation1.data?.service_items?.package_items?[section].items?.count ?? 0
            }
        }else {             print(reservation.data?.service_items?.items?.count ?? 0)
             return reservation.data?.service_items?.items?.count ?? 0
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userData.Instance.serviceType == "inside" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cart2Cell1", for: indexPath) as!  Cart2ContentCell
            let package = reservation1.data?.service_items?.package_items?.count ?? 0
            if  package > 0 {
                let service = reservation1.data?.service_items?.items?.count ?? 0
                if service > 0 {
                    if indexPath.section == (reservation1.data?.service_items?.package_items?.count ?? 0) {
                        cell.deleteBtn.isEnabled = true
                        cell.deleteBtn.isHidden = false
                        cell.updateViews(service:reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData())
                        currentService = reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData()
                        
                        cell.DeleteServiceCallBack = {
                            () -> () in
                            self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                            self.reservation1.data?.service_items?.items?.remove(at: indexPath.row)
                            self.tableView.reloadData()
                            
                        }
                        cell.addEmployeeServiceCallBack = {
                            (employeeID:Int) -> () in
                            if let serviceForItem = self.reservation1.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVation1Id)
                            }
                            //                       if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
                            //                        }
                            self.tableView.reloadData()
                        }
                        cell.addTimerServiceCallBack = {
                            (selectedTime:String) -> () in
                            if let serviceForItem = self.reservation1.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeTimer(itemId:serviceForItem , time : selectedTime, identifierId: self.reserVation1Id)
                            }
                            //                        if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
                            //                        }
                            self.tableView.reloadData()
                        }
                    }else{
                        cell.deleteBtn.isEnabled = false
                        cell.deleteBtn.isHidden = true
                        cell.updateViews(service:reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData())
                        currentService = reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData()
                        cell.DeleteServiceCallBack = {
                            () -> () in
                            self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                            self.reservation1.data?.service_items?.package_items?[indexPath.section].items?.remove(at: indexPath.row)
                            
                            self.tableView.reloadData()
                            
                        }
                        cell.addEmployeeServiceCallBack = {
                            (employeeID:Int) -> () in
                            if let serviceForItem = self.reservation1.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVation1Id)
                            }
                            //                       if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
                            //                        }
                            self.tableView.reloadData()
                        }
                        cell.addTimerServiceCallBack = {
                            (selectedTime:String) -> () in
                            if let serviceForItem = self.reservation1.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeTimer(itemId:serviceForItem , time : selectedTime, identifierId: self.reserVation1Id)
                            }
                            //                        if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
                            //                        }
                            self.tableView.reloadData()
                        }
                    }
                }else{
                    cell.deleteBtn.isEnabled = false
                    cell.deleteBtn.isHidden = true; cell.updateViews(service:reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData())
                    currentService = reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData()
                    cell.addEmployeeServiceCallBack = {
                        (employeeID:Int) -> () in
                        
                        if let serviceForPackageInItem = self.reservation1.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVation1Id)
                        }
                        self.tableView.reloadData()
                    }
                    cell.addTimerServiceCallBack = {
                        (selectedTime:String) -> () in
                        if let serviceForPackageInItem = self.reservation1.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVation1Id)
                        }
                        self.tableView.reloadData()
                    }
                    cell.DeleteServiceCallBack = {
                        () -> () in
                        self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                        self.reservation1.data?.service_items?.package_items?[indexPath.section].items?.remove(at: indexPath.row)
                        
                        self.tableView.reloadData()
                    }
                }
            }else{
                cell.deleteBtn.isEnabled = true
                cell.deleteBtn.isHidden = false;
                let service = reservation1.data?.service_items?.items?.count ?? 0
                if service > 0 {
                    if indexPath.section == (reservation1.data?.service_items?.package_items?.count ?? 0) {
                        cell.updateViews(service:reservation1.data?.service_items?.items?[indexPath.section] ?? ServicesData())
                        currentService = reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData()
                        cell.addEmployeeServiceCallBack = {
                            (employeeID:Int) -> () in
                            if let serviceForItem = self.reservation1.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVation1Id)
                            }
                            //                       if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeId(itemId: serviceForPackageInItem, employeeId : employeeID, identifierId: self.reserVationId)
                            //                        }
                            self.tableView.reloadData()
                        }
                        cell.addTimerServiceCallBack = {
                            (selectedTime:String) -> () in
                            if let serviceForItem = self.reservation1.data?.service_items?.items?[indexPath.row].id { self.SetEmployeeTimer(itemId:serviceForItem , time : selectedTime, identifierId: self.reserVation1Id)
                            }
                            //                        if let serviceForPackageInItem = self.reservation.data?.packages?[indexPath.section].items?[indexPath.row].id{ self.SetEmployeeTimer(itemId: serviceForPackageInItem, time : selectedTime, identifierId: self.reserVationId)
                            //                        }
                            self.tableView.reloadData()
                        }
                        cell.DeleteServiceCallBack = {
                            () -> () in
                            self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                            self.reservation1.data?.service_items?.items?.remove(at: indexPath.row)
                            
                            self.tableView.reloadData()
                            
                        }
                    }
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cart1Cell1", for: indexPath) as!  Cart1ContentCell
            cell.updateViews(service:reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData())
            currentService = reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData()
            if dateTextField1.text == "" {
                cell.chooseEmployee.isEnabled = false
            }else{
                cell.chooseEmployee.isEnabled = true
            }
            cell.addEmployeeServiceCallBack = {
                (employeeID:Int) -> () in
                if let serviceForItem = self.reservation.data?.service_items?.items?[indexPath.row].id {
                    if userData.Instance.serviceType == "inside"{
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                            self.SetEmployeeId(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVationId)              }else{
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                             self.SetEmployeeId1(itemId:serviceForItem , employeeId : employeeID, identifierId: self.reserVationId)              }
               
                }
                self.tableView.reloadData()
            }
            cell.DeleteServiceCallBack = {
                () -> () in
                if userData.Instance.serviceType == "inside"{
                    self.reserVation1Id = userData.Instance.identifierInside ?? -1
    self.deleteServiceInReservationShopData1(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)              }else{
                    self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                     self.deleteServiceInReservationShopData1(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)              }
               
                self.reservation.data?.service_items?.items?.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "Cart2Cell11") as! Cart2HeaderCell
        headerCell.updateViews(Package: reservation1.data?.service_items?.package_items?[section] ?? Package_item())
        headerCell.DeleteServiceCallBack = {
            () -> () in
            self.deletePackageInReservationShopData(id:self.reservation1.data?.service_items?.package_items?[section].id ?? -1 , userId : self.reserVation1Id)
            self.reservation1.data?.service_items?.package_items?.remove(at: section)
            self.tableView.reloadData()
            
            
        }
        
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if userData.Instance.serviceType == "inside" {
            let package = reservation1.data?.service_items?.package_items?.count ?? 0
            if  package > 0 {
                let service = reservation1.data?.service_items?.items?.count ?? 0
                if service > 0 {
                    if section == (reservation1.data?.service_items?.package_items?.count ?? 0) {
                        return CGFloat(0)
                    }
                    return CGFloat(50)
                }
            }else{
                return CGFloat(0)
            }
            return CGFloat(50)
         }else{
            return CGFloat(0)
        }
    }
    func SetEmployeeId(itemId:Int , employeeId:Int ,identifierId:Int ){
        self.reservation1 = Reservation()
        var header = APIs.Instance.getHeader()
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        print(APIs.Instance.setEmployeeId(id:itemId, identifier: identifierId))
        print(header)
        HUD.show(.progress)
        let par = ["employee_id": employeeId] as [String : Any];
        print(par)
        Alamofire.request(APIs.Instance.setEmployeeId(id:itemId, identifier: identifierId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
            .responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            
                        }catch{
                            print(error)
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
        }
    }
    func SetEmployeeTimer(itemId:Int , time:String,identifierId:Int ){
        var header = APIs.Instance.getHeader()
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        print(header)
        HUD.show(.progress)
        let par = ["due_time": time] as [String : Any];
        print(par)
        Alamofire.request(APIs.Instance.setDuetime(id:itemId, identifier: identifierId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
            .responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            
                        }catch{
                            print(error)
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
        }
    }
    func deletePackageInReservationShopData(id:Int , userId : Int){
        var header = APIs.Instance.getHeader()
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemovePackage(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
    func deleteServiceInReservationShopData(id:Int , userId : Int){
        var header = APIs.Instance.getHeader()
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        let url = (APIs.Instance.RemoveService(id: id, identifier: userId))
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                print(url)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData(id:Int){
        var header = APIs.Instance.getHeader()
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingSendDateForCurrentReservation(id:Int){
        var header = APIs.Instance.getHeader()
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        print(header)
        let par = ["date": dateTextField.text!] as [String : Any];
        print(par)
        print(APIs.Instance.SendDateForCurrentReservation(id:reserVationId));
   userData.Instance.fetchUser()
        reserVation1Id = userData.Instance.identifierInside ?? -1; Alamofire.request(APIs.Instance.SendDateForCurrentReservation(id:reserVationId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
            .responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            
                        }catch{
                            print(error)
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
        }
    }
    func confirmReservationShopData(userId : Int){
        var header = APIs.Instance.getHeader()
        if userData.Instance.serviceType == "inside" {
            let identifier = String(describing: userData.Instance.identifierInside)
            header.updateValue(identifier, forKey: "reservation_id")
        }else{
            let identifier = String(describing: userData.Instance.identifierOutside)
            header.updateValue(identifier, forKey: "reservation_id")
        }
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.ConfirmReservation(identifier: userId))
        Alamofire.request(APIs.Instance.ConfirmReservation(identifier: userId) , method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        self.performSegue(withIdentifier: "popOver", sender: self)
                        if userData.Instance.serviceType == "inside" {
                            userData.Instance.removeIdentifierInside()
                            userData.Instance.removecurrentReserveShopId()
                            self.reservation.data?.service_items?.items?.removeAll()
                            self.reservation.data?.packages?.removeAll()
                            self.tableView.reloadData()
                        }else{
                            userData.Instance.removeidentifierOutside()
                            userData.Instance.removecurrentReserveShopId()
                            self.reservation1.data?.service_items?.items?.removeAll()
                            self.reservation1.data?.packages?.removeAll()
                            self.tableView.reloadData()
                        }
                    }catch{
                        print(error)
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    
        func SetEmployeeId1(itemId:Int , employeeId:Int ,identifierId:Int ){
            var header = APIs.Instance.getHeader()
            if userData.Instance.serviceType == "inside" {
                let identifier = String(describing: userData.Instance.identifierInside)
                header.updateValue(identifier, forKey: "reservation_id")
            }else{
                let identifier = String(describing: userData.Instance.identifierOutside)
                header.updateValue(identifier, forKey: "reservation_id")
            }
            header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
            print(APIs.Instance.setEmployeeId(id:itemId, identifier: identifierId))
            print(header)
            HUD.show(.progress)
            let par = ["employee_id": employeeId] as [String : Any];
            print(par)
            Alamofire.request(APIs.Instance.setEmployeeId(id:itemId, identifier: identifierId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
                .responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(let value):
                        print(value)
                        HUD.hide()
                        print(value)
                        let temp = response.response?.statusCode ?? 400
                        if temp >= 300 {
                            
                            do {
                                let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                                self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                                print(err.errors)
                            }catch{
                                print("errorrrrelse")
                            }
                        }else{
                            do {
                                self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                                
                            }catch{
                                print(error)
                                HUD.flash(.label("Error Try Again"), delay: 1.0)
                            }
                        }
                    case .failure(_):
                        HUD.hide()
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                        break
                    }
            }
        }
        func SetEmployeeTimer1(itemId:Int , time:String,identifierId:Int ){
            var header = APIs.Instance.getHeader()
            if userData.Instance.serviceType == "inside" {
                let identifier = String(describing: userData.Instance.identifierInside)
                header.updateValue(identifier, forKey: "reservation_id")
            }else{
                let identifier = String(describing: userData.Instance.identifierOutside)
                header.updateValue(identifier, forKey: "reservation_id")
            }
            header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
            print(header)
            HUD.show(.progress)
            let par = ["due_time": time] as [String : Any];
            print(par)
            Alamofire.request(APIs.Instance.setDuetime(id:itemId, identifier: identifierId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
                .responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(let value):
                        print(value)
                        HUD.hide()
                        print(value)
                        let temp = response.response?.statusCode ?? 400
                        if temp >= 300 {
                            
                            do {
                                let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                                self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                                print(err.errors)
                            }catch{
                                print("errorrrrelse")
                            }
                        }else{
                            do {
                                self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                                
                            }catch{
                                print(error)
                                HUD.flash(.label("Error Try Again"), delay: 1.0)
                            }
                        }
                    case .failure(_):
                        HUD.hide()
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                        break
                    }
            }
        }
        func deleteServiceInReservationShopData1(id:Int , userId : Int){
            var header = APIs.Instance.getHeader()
            if userData.Instance.serviceType == "inside" {
                let identifier = String(describing: userData.Instance.identifierInside)
                header.updateValue(identifier, forKey: "reservation_id")
            }else{
                let identifier = String(describing: userData.Instance.identifierOutside)
                header.updateValue(identifier, forKey: "reservation_id")
            }
            let url = (APIs.Instance.RemoveService(id: id, identifier: userId))
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    print(url)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            
                        }catch{
                            print(error)
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        func PreparingReservationShopData1(id:Int){
            var header = APIs.Instance.getHeader()
            if userData.Instance.serviceType == "inside" {
                let identifier = String(describing: userData.Instance.identifierInside)
                header.updateValue(identifier, forKey: "reservation_id")
            }else{
                let identifier = String(describing: userData.Instance.identifierOutside)
                header.updateValue(identifier, forKey: "reservation_id")
            }
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.CurrentReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            
                        }catch{
                            print(error)
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        func PreparingSendDateForCurrentReservation1(id:Int){
            var header = APIs.Instance.getHeader()
            if userData.Instance.serviceType == "inside" {
                let identifier = String(describing: userData.Instance.identifierInside)
                header.updateValue(identifier, forKey: "reservation_id")
            }else{
                let identifier = String(describing: userData.Instance.identifierOutside)
                header.updateValue(identifier, forKey: "reservation_id")
            }
            header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
            
            print(header)
            HUD.show(.progress, onView: self.view)
            let par = ["date": dateTextField.text!] as [String : Any];
            print(par)
            print(APIs.Instance.SendDateForCurrentReservation(id:reserVationId));
            Alamofire.request(APIs.Instance.SendDateForCurrentReservation(id:reserVationId), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
                .responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(let value):
                        print(value)
                        HUD.hide()
                        print(value)
                        let temp = response.response?.statusCode ?? 400
                        if temp >= 300 {
                            
                            do {
                                let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                                print(err.errors)
                            }catch{
                                print("errorrrrelse")
                            }
                        }else{
                            do {
                                self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                                 self.reservationForTime = try JSONDecoder().decode(ReservationForTime.self, from: response.data!)
                             
                                print(self.words2)
                                
                            }catch{
                                print(error)
                                HUD.flash(.label("Error Try Again"), delay: 1.0)
                            }
                        }
                    case .failure(_):
                        HUD.hide()
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.label(lockString), delay: 1.0)
                        break
                    }
            }
        }
        func confirmReservationShopData1(userId : Int){
            var header = APIs.Instance.getHeader()
            if userData.Instance.serviceType == "inside" {
                let identifier = String(describing: userData.Instance.identifierInside)
                header.updateValue(identifier, forKey: "reservation_id")
            }else{
                let identifier = String(describing: userData.Instance.identifierOutside)
                header.updateValue(identifier, forKey: "reservation_id")
            }
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.ConfirmReservation(identifier: userId) , method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                            print(err.errors)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            if userData.Instance.serviceType == "inside" {
                                userData.Instance.removeIdentifierInside()
                                self.reservation.data?.service_items?.items?.removeAll()
                                self.reservation.data?.packages?.removeAll()
                                self.tableView.reloadData()
                            }else{
                                userData.Instance.removeidentifierOutside()
                                self.reservation1.data?.service_items?.items?.removeAll()
                                self.reservation1.data?.packages?.removeAll()
                                self.tableView.reloadData()
                            }
                           
                        }catch{
                            print(error)
                            HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle:NSLocalizedString("Done", comment: "تم"), andButtons: nil)
        }
        
        @IBAction func confirmReservation(_ sender: Any) {
            if userData.Instance.token == "" || userData.Instance.token == nil {
                self.performSegue(withIdentifier: "popOver1111", sender: self)
            }else{
                if userData.Instance.serviceType == "inside" {
                    let userIdentifier = userData.Instance.identifierInside
                    print(userIdentifier)
                    confirmReservationShopData(userId: userIdentifier ?? -1)
                }else{
                        let userIdentifier = userData.Instance.identifierOutside
                        print(userIdentifier)
                        confirmReservationShopData(userId: userIdentifier ?? -1)
                
                    }
        }
    }
        @IBAction func changeLanguage(_ sender: Any) {
       
            if L102Language.currentAppleLanguage() == "ar"{
                L102Language.setAppleLAnguageTo(lang: "en")
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            } else {
                L102Language.setAppleLAnguageTo(lang: "ar")
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                
            };
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarControllerViewController")
            let mainwindow = (UIApplication.shared.delegate?.window!)!
            mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
            UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
            }) { (finished) -> Void in
            }
        }
    func setupSideMenu(){
        let view = storyboard?.instantiateViewController(withIdentifier: "RootViewController")
        
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: view!)
        let view2 = storyboard?.instantiateViewController(withIdentifier: "RootViewController")
        let leftNavigationController = UISideMenuNavigationController(rootViewController: view2!)
        
        
        leftNavigationController.leftSide = true
        SideMenuManager.menuLeftNavigationController = leftNavigationController
        
        menuLeftNavigationController.leftSide = false
        SideMenuManager.menuRightNavigationController = menuLeftNavigationController
    }
}

