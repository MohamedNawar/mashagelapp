//
//  AllBeautyCenterForAdminViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/9/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SDWebImage
import HCSStarRatingView
import SideMenu

class AllBeautyCenterForAdminViewController : UIViewController , UITableViewDelegate , UITableViewDataSource  {
    @IBAction func menue(_ sender: Any) {
        
        // log out
        userData.Instance.remove()
        userData.Instance.removeShopID()
        userData.Instance.fetchUser()
       
        makeErrorAlert(title: "Log out", SubTitle: "Are you sure you want to logout", Image: #imageLiteral(resourceName: "img34"))
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
     //   let navVc = UINavigationController(rootViewController: vc)
        self.present(vc, animated: true, completion: nil)
        
        
//        if L102Language.currentAppleLanguage() == "en" {
//            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
//            
//        }else{
//            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
//            
//        }
    }
    func setupSideMenu(){
        let view = storyboard?.instantiateViewController(withIdentifier: "RootViewController")
        
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: view!)
        let view2 = storyboard?.instantiateViewController(withIdentifier: "RootViewController")
        let leftNavigationController = UISideMenuNavigationController(rootViewController: view2!)
        
        
        leftNavigationController.leftSide = true
        SideMenuManager.menuLeftNavigationController = leftNavigationController
        
        menuLeftNavigationController.leftSide = false
        SideMenuManager.menuRightNavigationController = menuLeftNavigationController
    }
    var shopesForOwner = [ShopData](){
        didSet{
        }
    }
    var shopes = Shops(){
        didSet{
            let ownerId = userData.Instance.data?.id as! Int
            print(ownerId)
            shopesForOwner = shopes.data?.filter({return $0.owner?.id == ownerId}) ?? [ShopData]()
            tableView.reloadData()
        }
    }
    var request = "http://mashaghel.elnooronlineworks.com/api/home"
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //            let ownerId = userData.Instance.data?.id as! Int
        //
        //            shopesForOwner = shopes.data?.filter({return $0.owner?.id == ownerId}) ?? [ShopData]()
        print(shopesForOwner)
        return shopesForOwner.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let ShopID = shopesForOwner[indexPath.row].id {
            userData.Instance.removeShopID()
            print(ShopID)
            let currentID = ShopID as! Int
            userData.Instance.userSelectedShopId =  currentID
            print(currentID)
            UserDefaults.standard.set(currentID, forKey: "userSelectedShopId")
        }
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllBeautyCenterForAdminCell", for: indexPath)
        if let nameValue = shopes.data?[indexPath.row].name {
            let name = cell.viewWithTag(1) as! UILabel
            name.text = "\(nameValue)"
            if L102Language.currentAppleLanguage() == "ar" {
                name.textAlignment = .right
            }
        }
        if let addressValue = shopes.data?[indexPath.row].address {
            let address = cell.viewWithTag(2) as! UILabel
            address.text = "\(addressValue)"
            if L102Language.currentAppleLanguage() == "ar" {
                address.textAlignment = .right
            }
        }
        if let rateValue = shopes.data?[indexPath.row].review_count {
            let ratting = cell.viewWithTag(10) as! HCSStarRatingView
            ratting.value = CGFloat(rateValue)
        }
        if let rateAverageValue = shopes.data?[indexPath.row].review_average {
            let rateAverage = cell.viewWithTag(8) as! UILabel
            if let n = NumberFormatter().number(from: rateAverageValue) {
                let f = CGFloat(n)
                rateAverage.text = "\(f)"
            }
        }
        
        if let DISTANCEValue = shopes.data?[indexPath.row].distance?.SString {
            let distance = cell.viewWithTag(44) as! UILabel
            if let n = NumberFormatter().number(from: DISTANCEValue) {
                let f = CGFloat(n)
                distance.text = "\(f)"
                if L102Language.currentAppleLanguage() == "ar" {
                    distance.textAlignment = .right
                }
            }
        }
        if let  imageIndex = (shopes.data?[indexPath.row].images?.first?.large) {
            let image = cell.viewWithTag(5) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex), completed: nil)
        }
        if let addressValue = shopes.data?[indexPath.row].address {
            let address = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                address.textAlignment = .right
            }
            address.text = "\(addressValue)"
        }
        if let reviewCountValue = shopes.data?[indexPath.row].review_count {
            let address = cell.viewWithTag(9) as! UILabel
            address.text = "\(reviewCountValue)"
            if L102Language.currentAppleLanguage() == "ar" {
                address.textAlignment = .right
            }
        }
        
        return cell
    }
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
        userData.Instance.fetchUser()
        loadShopsData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadShopsData()
        if L102Language.currentAppleLanguage() == "ar" {
             self.title =  "محلاتي"
        }else{
            self.title = NSLocalizedString("My Shopes", comment: "محلاتي")
        }
    }
    func loadShopsData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(request , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopes = try JSONDecoder().decode(Shops.self, from: response.data!)
                        print(self.shopes)
                    }catch{
                        HUD.flash(.label("Error Try Again"), delay: 1.0)
                        print(error)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "ar"{
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        };
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarControllerViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
}
}


