//
//  PersonalProfileViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/6/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class PersonalProfileViewController: UIViewController {
    @IBOutlet weak var emailLbl: DesignableTextField!
    @IBOutlet weak var phoneLbl: DesignableTextField!
    @IBOutlet weak var nameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        userData.Instance.fetchUser()
        nameLbl.text = userData.Instance.data?.name
        phoneLbl.text = userData.Instance.data?.mobile
        emailLbl.text = userData.Instance.data?.email
        if L102Language.currentAppleLanguage() == "ar" {
            self.title = "الملف الشخصي"
            navigationItem.title = "الملف الشخصي"
        }else{
            self.title = NSLocalizedString("Personal Profile", comment:"الملف الشخصي")
            navigationItem.title = NSLocalizedString("Personal Profile", comment:"الملف الشخصي")        }

        tabBarItem.imageInsets.top = -6

    }
    
    
    
}
