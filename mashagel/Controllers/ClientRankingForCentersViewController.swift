//
//  ClientRankingForCentersViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import SideMenu

class ClientRankingForCentersViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientRankingForCentersCell", for: indexPath)
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        userData.Instance.fetchUser()

    }
}

