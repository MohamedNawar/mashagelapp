//
//  ChooseEmployeeServiceViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class ChooseEmployeeServiceViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate  {
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
        userData.Instance.fetchUser()

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CategoryCollectionView {
            return 9
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseEmployeeServiceCell", for: indexPath)
        return cell
    }
}
extension ChooseEmployeeServiceViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.CategoryCollectionView.frame.width / 3)  , height: 170 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}





