//
//  btn.swift
//  mashagel
//
//  Created by ElNoorOnline on 1/8/19.
//  Copyright © 2019 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Foundation

class btn: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

 
        if let image = self.currentImage?.withRenderingMode(.alwaysTemplate) {
            self.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
             self.setImage(image, for: .normal)
        }

    }
    
}


