//
//  DesignableTextField.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import FCAlertView
import SideMenu

@IBDesignable
class  DesignableTextField: UITextField  {
    
    @IBInspectable var leftImage:UIImage?{
        didSet{
            updateView()
        }
    }
    @IBInspectable var leftPadding:CGFloat = 0 {
        didSet{
            updateView()
        }
    }
    
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding - 10 , y: 0, width: 20, height: 20))
            imageView.image = image
            var width = leftPadding + 20
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width =  width + 5
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            self.leftView = view
            self.leftViewMode = UITextFieldViewMode.always
            self.placeholder = placeholder
        }else{
            leftViewMode = .never
        }
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder!: "", attributes:  [kCTForegroundColorAttributeName as NSAttributedStringKey : tintColor])
    }
    
}
extension UITextField {
    
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding(_ padding: PaddingSide) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

/* api call */

public enum ConnectionResult {
    case success(Data)
    case failure(Error)
    case error()
}

public func callApi(vc: UIViewController, withURL:String, method:HTTPMethod, headers:[String:String], Parameter:[String:Any],completionHandler:@escaping (_ success:ConnectionResult) -> Void) {
    Alamofire.request(withURL, method: method, parameters: Parameter, encoding: URLEncoding.default, headers: headers).responseJSON {
        (response:DataResponse) in
        print(response.value)
        switch(response.result) {
        case .success(let value):
            
            
            let temp = response.response?.statusCode ?? 400
            if temp >= 300 {
                print("eeeeee")
                do {
                    //Fail
                    let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                    completionHandler(.error())
                    if err.parseError() == "" {
                        
                    }
                     makeErrorAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                    
                    
                }catch{
                    completionHandler(.error())
                    makeErrorAlert(title: "Error", SubTitle: "please try again", Image: #imageLiteral(resourceName: "img34"))
                    
                    
                }
            }else{
                completionHandler(.success(response.data ?? Data()))
            }
            break
            
        case .failure(let error):
            completionHandler(.failure(error))
            //      print(response.result.error)
            
        }
    }
    //
}


public func makeErrorAlert(title: String, SubTitle: String, Image : UIImage) {
    let alert = FCAlertView()
    alert.avoidCustomImageTint = true
    let updatedFrame = alert.bounds
    alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
    alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
}



/* */
public func setupSideMenu(){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let view = storyboard.instantiateViewController(withIdentifier: "RootViewController")
    
    let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: view)
    let view2 = storyboard.instantiateViewController(withIdentifier: "RootViewController")
    let leftNavigationController = UISideMenuNavigationController(rootViewController: view2)
    
    
    leftNavigationController.leftSide = true
    SideMenuManager.menuLeftNavigationController = leftNavigationController
    
    menuLeftNavigationController.leftSide = false
    SideMenuManager.menuRightNavigationController = menuLeftNavigationController
}

/* to remove navigationBar_boarder */
public func setupNavBar(vc:UIViewController, isHidden :Bool){
    
    vc.navigationController?.setNavigationBarHidden(isHidden, animated: true)
    vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    vc.navigationController?.navigationBar.shadowImage = UIImage()
}

public func setupNavBar(vc:UIViewController){
    vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    vc.navigationController?.navigationBar.shadowImage = UIImage()
    vc.navigationController?.navigationBar.isTranslucent = true
    vc.navigationController?.navigationBar.titleTextAttributes =
        [NSAttributedStringKey.font:  UIFont(name: "NeoSansArabic", size: 16)! ,
         NSAttributedStringKey.foregroundColor: UIColor.white
    ]
    vc.navigationController?.navigationBar.tintColor = .white
}


public func statusBar_Setup(){
    
    
    
    UIApplication.shared.statusBarStyle = .lightContent
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    statusBar.backgroundColor = mainColor
    statusBar.isOpaque = false
    let ToolBarAppearace = UIToolbar.appearance()
   let BarAppearace = UINavigationBar.appearance()
    BarAppearace.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
 BarAppearace.backgroundColor = mainColor
  BarAppearace.tintColor = UIColor.white
////    BarAppearace.isTranslucent = true
//
    BarAppearace.titleTextAttributes = [
        NSAttributedStringKey.font: NormalFont!,
        NSAttributedStringKey.foregroundColor: UIColor.white
    ]
  ToolBarAppearace.backgroundColor = mainColor
  ToolBarAppearace.tintColor = UIColor.white
//    let ButtonBarAppearace = UIBarButtonItem.appearance()
//    ButtonBarAppearace.setTitleTextAttributes([
//        NSAttributedStringKey.font: NormalFont as Any
//        ], for: UIControlState.normal)
//    ButtonBarAppearace.tintColor = UIColor.white
    
    
//    UIApplication.shared.statusBarStyle = .lightContent
//    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//    statusBar.backgroundColor = mainColor
//    let ToolBarAppearace = UIToolbar.appearance()
//    let BarAppearace = UINavigationBar.appearance()
//    BarAppearace.backgroundColor = mainColor
//    BarAppearace.tintColor = UIColor.white
//    BarAppearace.isTranslucent = true
//
//    BarAppearace.titleTextAttributes = [
//        NSAttributedStringKey.font: NormalFont!,
//        NSAttributedStringKey.foregroundColor: UIColor.white
//    ]
//    ToolBarAppearace.backgroundColor = mainColor
//    ToolBarAppearace.tintColor = UIColor.white
//    let ButtonBarAppearace = UIBarButtonItem.appearance()
//    ButtonBarAppearace.setTitleTextAttributes([
//        NSAttributedStringKey.font: NormalFont as Any
//        ], for: UIControlState.normal)
//    ButtonBarAppearace.tintColor = UIColor.white
}

//public func makeErrorAlert(_ vc: UIViewController, subTitle: String) {
//
//
//    let alert = UIAlertController(title: "", message: subTitle, preferredStyle: .alert)
//    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//        switch action.style{
//        case .default:
//            print("default")
//
//        case .cancel:
//            print("cancel")
//
//        case .destructive:
//            print("destructive")
//
//
//        }}))
//    vc.present(alert, animated: true, completion: nil)
//}
