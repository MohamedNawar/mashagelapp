//
//  AppDelegate.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import FCAlertView
import SideMenu
import GoogleSignIn
import TwitterKit
import OneSignal
import FBSDKCoreKit

var mainColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
let NormalFont = UIFont(name: "Cairo", size: 17)
let BoldFont = UIFont(name: "Cairo-Bold", size: 17)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , GIDSignInDelegate, OSSubscriptionObserver{
    
    
    var window: UIWindow?
    private var reachability : Reachability!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        //navigationBar Appearance
        statusBar_Setup()
        
        // google login keys
        GIDSignIn.sharedInstance()?.clientID = "581610152564-83esg747g2512acgrqnljstj5slmjirk.apps.googleusercontent.com"
        GIDSignIn.sharedInstance()?.serverClientID = "581610152564-bmt26uvbmg2jv1fof23oh36le91hvkj8.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        // Twitter login
        TWTRTwitter.sharedInstance().start(withConsumerKey:"6jTue7WlMOP3nXjtcnvfnuXU2", consumerSecret:"Q5bDqFtQRFtJIkkLdTG1GMA9AHdhGPuiCKYLfTG8BVUTmLRWKJ")

        
        //one signal
            let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
            
            // Replace 'YOUR_APP_ID' with your OneSignal App ID.
            OneSignal.initWithLaunchOptions(launchOptions,
                                            appId: "3d3fd3d5-bbfa-4cc6-b136-a87455201f38",
                                            handleNotificationAction: nil,
                                            settings: onesignalInitSettings)
            
            OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
            
            // Recommend moving the below line to prompt for push after informing the user about
            //   how your app will use them.
            OneSignal.promptForPushNotifications(userResponse: { accepted in
                print("User accepted notifications: \(accepted)")
            })
        OneSignal.add(self as OSSubscriptionObserver)
        
        
        //google map
        GMSServices.provideAPIKey("AIzaSyB5bqJ6WM0DJcdmNdHnVc_ZQRO2GVb9AV4")
        
        userData.Instance.fetchUser()
       L102Localizer.DoTheMagic()
        let attr = NSDictionary(object: UIFont(name: "Cairo", size: 12.0)!, forKey: NSAttributedStringKey.font as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject : AnyObject] , for: .normal)
        
        if L102Language.currentAppleLanguage() == "ar" {
        }
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        // Override point for customization after application launch.
        //        L102Localizer.DoTheMagic()
       
        
        //Keybored Setup
        IQKeyboardManager.shared.enable = true
        
        //For checking the Internet
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        self.reachability = Reachability.init()
        do {
            try self.reachability.startNotifier()
        } catch {
            
        }
        
        // if logged in
        if userData.Instance.token != nil {
            if userData.Instance.token != "" {
                
                if userData.Instance.data?.type == 2 {
                    rootView(idintifier: "AllBeautyCenterForAdminViewController", true)
                }else{
                    rootView(idintifier: "TabBarControllerViewController", false)
                }
                
            }
            
        }else{
            print("not log in")
        }
 
        
        
        
        /* FACEBOOK LOGIN        */
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    func rootView(idintifier : String, _ admin:Bool){
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: idintifier)
     
     
        if admin {
            let vcc = UINavigationController(rootViewController: vc)
            self.window?.rootViewController = vcc
        }else {
            self.window?.rootViewController = vc
        }
        
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //Get called when the wifi statue changed
    @objc func reachabilityChanged(notification:Notification) {
        let reachability = notification.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            makeDoneAlert(title: "من فضلك قم باعادة الاتصال بالانترنت مرة اخري", SubTitle: "", Image: #imageLiteral(resourceName: "img13"), color: UIColor.red)
            print("Network not reachable")
        }
    }
    
    // the alert to be Poped Up
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    // MARK: google login
    // [START openurl]
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    
       
 
//        GIDSignIn.sharedInstance().handle(url,
//                                                                   sourceApplication: sourceApplication,
//                                                                   annotation: annotation)
//
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        return facebookDidHandle
    }
    
    
    // [END openurl]
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
       // return GIDSignIn.sharedInstance().handle(url,sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        let facebookDidHandle =   FBSDKApplicationDelegate.sharedInstance()?.application(app, open: url, sourceApplication: (options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String), annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        
        return facebookDidHandle!
    }

    
    // [START signin_handler]
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            // [START_EXCLUDE silent]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
        } else {
            // Perform any operations on signed in user here.
            
            let idToken = user.authentication.idToken // Safe to send to the server
            
            // [START_EXCLUDE]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"),
                object: nil,
                //       userInfo: ["statusText": "Signed in user:\n\(fullName)"])
                userInfo: ["statusText": "\(String(describing: idToken!))"])
            
            // [END_EXCLUDE]
        }
    }
    // [END signin_handler]
    // [START disconnect_handler]
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }
    // [END disconnect_handler]
    
    
    /* one signal */
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "onesignalid")
            print("ooo \(UserDefaults.standard.string(forKey: "onesignalid"))")
            print("Current playerId \(playerId)")
            
            
        }
    }

    
}

